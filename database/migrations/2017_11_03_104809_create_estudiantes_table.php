<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateEstudiantesTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('estudiantes', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nombres');
            $table->string('apellidos');
            $table->string('di');
            $table->string('direccion');
            $table->integer('municipio_id')->unsigned();
            $table->string('nombree_acudiente');
            $table->string('apellidos_acudiente');
            $table->string('telefono_acudiente');
            $table->string('celular_acudiente');
            $table->string('grado');
            $table->string('grupo');
            $table->integer('edad');
            $table->text('observaciones');
            $table->string('tipo_rh');
            $table->timestamps();
            $table->softDeletes();
            $table->integer('user_id')->unsigned();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('estudiantes');
    }
}
