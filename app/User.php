<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use App\Traits\DatesTranslator;
use App\Notifications\ResetPasswordNotification;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\SoftDeletes;

class User extends Authenticatable
{
    use Notifiable, SoftDeletes;
    use DatesTranslator; 

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['nombres', 'apellidos', 'cedula', 'avatar', 'email', 'password', ];
    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * Send the password reset notification.
     *
     * @param  string  $token
     * @return void
     */
    public function sendPasswordResetNotification($token)
    {
        $this->notify(new ResetPasswordNotification($token));
    }

    /**
     * Relationships
     */
   
    public function roles()
    {
        return $this->belongsToMany('App\Models\Role', 'assigned_roles')->withTimestamps();
    }


    public function docente()
    {
        return $this->hasOne('App\Models\Docente', 'cedula', 'cedula');
    }


    // public function natural()
    // {
    //     return $this->hasOne('App\Models\Natural', 'cedula', 'cedula');
    // }
    /**
     * Functions security
     */
    public function hasRoles(array $roles)
    {        
        return $this->roles->pluck('name')->intersect($roles)->count();        
    }

    public function isAdmin()
    {
        return $this->hasRoles(['admin']);
    }

    public function isMixAdmin()
    {
        return $this->hasRoles(['admin', 'ceo']);
    }  

    public function isWork()
    {
        return $this->hasRoles(['admin', 'ceo', 'secretary']);
    }  

     /**
     * Mutadores & Ascensores
     */     

    public function getFullNameAttribute()
    {
       return $this->nombres . ' ' . $this->apellidos;
    }

     /**
     * seleccionadores
     */
    public static function selUsuario($role){
        $array['']= "seleccione...";
        $modelo = User::all()->toArray();
            foreach ($modelo as $key => $value) {
                if ($value['role']==$role) {
                    $array[$value['id']] = $value['nombres'].' '.$value['apellidos'];
                }                
            }
        return ($array);
    }

}
