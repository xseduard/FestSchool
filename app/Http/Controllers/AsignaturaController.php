<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateAsignaturaRequest;
use App\Http\Requests\UpdateAsignaturaRequest;
use App\Repositories\CentralRepository;
use App\Repositories\AsignaturaRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;
use Session;

class AsignaturaController extends AppBaseController
{
    /** @var  AsignaturaRepository */
    private $asignaturaRepository;
    private $centralRepository;

    public function __construct(AsignaturaRepository $asignaturaRepo, CentralRepository $centralRepo)
    {
        $this->middleware('auth');
        $this->middleware('roles:admin,supervisor');
        $this->asignaturaRepository = $asignaturaRepo;
        $this->centralRepository = $centralRepo;
    }

    /**
     * Display a listing of the Asignatura.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->asignaturaRepository->pushCriteria(new RequestCriteria($request));
        $asignaturas = $this->asignaturaRepository->orderBy('updated_at', 'desc')->paginate();

        return view('asignaturas.index')
            ->with('asignaturas', $asignaturas);
    }
    /**
     * Commons Funtions Repository
     */
    public function sels()
    {
        $sels = [];
        // $sels['id_attribute'] = $this->centralRepository->id_attribute();
        return $sels;
    }

    /**
     * Show the form for creating a new Asignatura.
     *
     * @return Response
     */
    public function create()
    {
        $sels = $this->sels();

        return view('asignaturas.create')->with(['sels' => $sels]);
    }

    /**
     * Store a newly created Asignatura in storage.
     *
     * @param CreateAsignaturaRequest $request
     *
     * @return Response
     */
    public function store(CreateAsignaturaRequest $request)
    {
        $input = $request->all();
        $input['user_id'] = \Auth::id();

        $asignatura = $this->asignaturaRepository->create($input);

        Session::flash('success', 'Asignatura registrado correctamente.');

        return redirect(route('asignaturas.index'));
    }

    /**
     * Display the specified Asignatura.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $asignatura = $this->asignaturaRepository->findWithoutFail($id);

        if (empty($asignatura)) {
            Session::flash('error', 'Asignatura No se encuentra registrado.');

            return redirect(route('asignaturas.index'));
        }

        return view('asignaturas.show')->with('asignatura', $asignatura);
    }

    /**
     * Show the form for editing the specified Asignatura.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $asignatura = $this->asignaturaRepository->findWithoutFail($id);

        if (empty($asignatura)) {
            Session::flash('error', 'Asignatura No se encuentra registrado.');

            return redirect(route('asignaturas.index'));
        }

         $sels = $this->sels();

        return view('asignaturas.edit')->with(['asignatura' => $asignatura, 'sels' => $sels]);
    }

    /**
     * Update the specified Asignatura in storage.
     *
     * @param  int              $id
     * @param UpdateAsignaturaRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateAsignaturaRequest $request)
    {
        $asignatura = $this->asignaturaRepository->findWithoutFail($id);

        if (empty($asignatura)) {
            Session::flash('error', 'Asignatura No se encuentra registrado.');

            return redirect(route('asignaturas.index'));
        }

        $input = $request->all();
        $input['user_id'] = \Auth::id();

        $asignatura = $this->asignaturaRepository->update($input, $id);

        Session::flash('success', 'Asignatura actualizado correctamente.');

        return redirect(route('asignaturas.index'));
    }

    /**
     * Remove the specified Asignatura from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $asignatura = $this->asignaturaRepository->findWithoutFail($id);

        if (empty($asignatura)) {
            Session::flash('error', 'Asignatura No se encuentra registrado.');

            return redirect(route('asignaturas.index'));
        }

        $this->asignaturaRepository->delete($id);

        Session::flash('success', 'Asignatura eliminado correctamente.');

        return redirect(route('asignaturas.index'));
    }
}
