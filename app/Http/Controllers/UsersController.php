<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateUserRequest;
use App\Http\Requests\UpdateUserRequest;
use App\User;
use App\Models\Role;
use Flash;
use Illuminate\Http\Request;
use Mail;
use Illuminate\Support\Facades\Storage;

class UsersController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        // $this->middleware('roles:admin,gerente',['except' => ['edit', 'update']]);
        // $this->middleware([
        //     'auth', 'roles:admin'
        // ]);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::with(['roles'])->paginate(15);
        return view('users.index')->with(['users' => $users]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $roles = Role::pluck('display_name', 'id');
        return view('users.create')->with(['roles' => $roles]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateUserRequest $request)
    {
        $input = [
            'nombres'   => $request->nombres,
            'apellidos' => $request->apellidos,
            'cedula'    => $request->cedula,
            'email'     => $request->email,
            'password'  => bcrypt($request->password),
        ];

        $user = (new User)->fill($input);

        if ($request->hasFile('avatar')) 
        {
            $user->avatar = Storage::url( $request->file('avatar')->store('public/avatars') );
        }

        $user->save();

        Flash::success('Usuario registrado correctamente.');

        return redirect(route('usuarios.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        
        try {
            $user =  User::find($id);
        } catch (Exception $e) { /*nothing*/ }

        if (empty($user)) {
            Flash::error('Usuario No se encuentra registrado.');

            return redirect(route('usuarios.index'));
        }
        
        // $this->authorize($user);

        $roles = Role::pluck('display_name', 'id');

        return view('users.edit')->with(['user' => $user, 'roles' => $roles]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function resetImage($id)
    {
        try {
            $user = User::find($id);
        } catch (Exception $e) { /*nothing*/ }

        if (empty($user)) {
            Flash::error('Usuario No se encuentra registrado.');

            return redirect(route('usuarios.index'));
        }

        // $this->authorize($user);

                 
       if ($user->avatar != '/default-profile.png') 
       {
            $photoPath = str_replace('storage', 'public', $user->avatar);

            Storage::delete($photoPath);

        $user->update(['avatar' => '/default-profile.png']);
        
        Flash::success('Avatar Reestablecido.');

       } else {

        Flash::warning('Avatar por defecto.');

       }

        return redirect(route('usuarios.edit', [$id]));
            
        
    }
    public function update(UpdateUserRequest $request, $id)
    {
        
        try {
            $user = User::find($id);
        } catch (Exception $e) { /*nothing*/ }

        if (empty($user)) {
            Flash::error('Usuario No se encuentra registrado.');

            return redirect(route('usuarios.index'));
        }
        
        // $this->authorize($user);

        if ($request->hasFile('avatar')) 
        {           
           if ($user->avatar != '/default-profile.png') 
           {
                $photoPath = str_replace('storage', 'public', $user->avatar);

                Storage::delete($photoPath);
           }

            $user->avatar = Storage::url( $request->file('avatar')->store('public/avatars') );
        }

        $user->update($request->only('cedula', 'nombres', 'apellidos', 'email'));
        
        if (auth()->user()->isMixAdmin()) {
            
            $assignedRoles = $request->roles;

            if (!is_null($request->roles)) {
                foreach ($request->roles as $role) {
                   $assignedRoles[$role] = ['who_user' => \Auth::id()];
                }
            }
            $user->roles()->sync($assignedRoles);

            Flash::success('Usuario actualizado correctamente.');

            return redirect('usuarios');
        }

        
        Flash::success('Usuario actualizado correctamente.');

        return redirect('home');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

        try {
            $user = User::find($id);
        } catch (Exception $e) { /*nothing*/ }

        if (empty($user)) {
            Flash::error('Usuario No se encuentra registrado.');

            return redirect(route('usuarios.index'));
        }
        
        // $this->authorize($user);

        $user->delete();

        Flash::success('Usuario eliminado correctamente.');

        return redirect(route('usuarios.index'));
    }

    public function genUser($id)
    {
        $natural = \App\Models\Natural::find($id);

        if (empty($natural->email)) {
            Flash::error('Tercero natural: '. $natural->fullname.' No posee registro de "email" para realizar esta acción.');

            return redirect(route('naturals.index'));
        }


       $result = User::create([
            'nombres'   => $natural->nombres,
            'apellidos' => $natural->apellidos,
            'cedula'    => $natural->cedula,
            'email'     => $natural->email,
            'password'  => bcrypt('FE$T27VISOR'),
        ]);

       $result->roles()->sync([3]);

       try{
            Mail::send('emails.genUser', ['natural' => $natural, 'pass' => 'FE$T27VISOR'], function ($message) use($natural){
     
                $message->from('sistema@transportedigital.com', 'FestSupervisor');
         
                $message->to($natural->email)->subject('Alcaldia de Apartadó');
         
            });
        } catch(\Exception $e) {
            // errors
        }



       Flash::success('Usuario Generado correctamente.');

        return redirect(route('naturals.index'));
    }
}
