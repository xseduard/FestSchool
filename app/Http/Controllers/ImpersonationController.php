<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Laracasts\Flash\Flash;

class ImpersonationController extends Controller
{
   
    public function __construct()
    {
        $this->middleware('auth');
        // $this->middleware('roles:admin');       
    }

    public function store()
    {
    	if (auth()->id() === 1) {
	    	session(['impersonator_id' => auth()->id()]);

	      	auth()->loginUsingId( request('user_id') );

	        Flash::success( 'Estas personificando al usuario id:'.request('user_id') );

	        return redirect(route('home'));
    	}  else {
    		Flash::error('Acción no permitida.');

            return redirect(route('usuarios.index'));
    	}   	
    }

    public function destroy()
    {        	
      	auth()->loginUsingId( session('impersonator_id') );
      	session()->forget('impersonator_id');

        Flash::success( 'Has dejado de personificar' );

        return redirect(route('home'));
    }

}
