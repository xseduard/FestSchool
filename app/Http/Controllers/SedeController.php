<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateSedeRequest;
use App\Http\Requests\UpdateSedeRequest;
use App\Repositories\CentralRepository;
use App\Repositories\SedeRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;
use Session;

class SedeController extends AppBaseController
{
    /** @var  SedeRepository */
    private $sedeRepository;
    private $centralRepository;

    public function __construct(SedeRepository $sedeRepo, CentralRepository $centralRepo)
    {
        $this->middleware('auth');
        $this->middleware('roles:admin,supervisor');
        $this->sedeRepository = $sedeRepo;
        $this->centralRepository = $centralRepo;
    }

    /**
     * Display a listing of the Sede.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->sedeRepository->pushCriteria(new RequestCriteria($request));
        $sedes = $this->sedeRepository->orderBy('updated_at', 'desc')->paginate();

        return view('sedes.index')
            ->with('sedes', $sedes);
    }
    /**
     * Commons Funtions Repository
     */
    public function sels()
    {
        $sels = [];
        // $sels['id_attribute'] = $this->centralRepository->id_attribute();
        return $sels;
    }

    /**
     * Show the form for creating a new Sede.
     *
     * @return Response
     */
    public function create()
    {
        $sels = $this->sels();

        return view('sedes.create')->with(['sels' => $sels]);
    }

    /**
     * Store a newly created Sede in storage.
     *
     * @param CreateSedeRequest $request
     *
     * @return Response
     */
    public function store(CreateSedeRequest $request)
    {
        $input = $request->all();
        $input['user_id'] = \Auth::id();

        $sede = $this->sedeRepository->create($input);

        Session::flash('success', 'Sede registrado correctamente.');

        return redirect(route('sedes.index'));
    }

    /**
     * Display the specified Sede.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $sede = $this->sedeRepository->findWithoutFail($id);

        if (empty($sede)) {
            Session::flash('error', 'Sede No se encuentra registrado.');

            return redirect(route('sedes.index'));
        }

        return view('sedes.show')->with('sede', $sede);
    }

    /**
     * Show the form for editing the specified Sede.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $sede = $this->sedeRepository->findWithoutFail($id);

        if (empty($sede)) {
            Session::flash('error', 'Sede No se encuentra registrado.');

            return redirect(route('sedes.index'));
        }

         $sels = $this->sels();

        return view('sedes.edit')->with(['sede' => $sede, 'sels' => $sels]);
    }

    /**
     * Update the specified Sede in storage.
     *
     * @param  int              $id
     * @param UpdateSedeRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateSedeRequest $request)
    {
        $sede = $this->sedeRepository->findWithoutFail($id);

        if (empty($sede)) {
            Session::flash('error', 'Sede No se encuentra registrado.');

            return redirect(route('sedes.index'));
        }

        $input = $request->all();
        $input['user_id'] = \Auth::id();

        $sede = $this->sedeRepository->update($input, $id);

        Session::flash('success', 'Sede actualizado correctamente.');

        return redirect(route('sedes.index'));
    }

    /**
     * Remove the specified Sede from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $sede = $this->sedeRepository->findWithoutFail($id);

        if (empty($sede)) {
            Session::flash('error', 'Sede No se encuentra registrado.');

            return redirect(route('sedes.index'));
        }

        $this->sedeRepository->delete($id);

        Session::flash('success', 'Sede eliminado correctamente.');

        return redirect(route('sedes.index'));
    }
}
