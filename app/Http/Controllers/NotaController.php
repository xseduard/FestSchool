<?php

namespace App\Http\Controllers;

use App\Http\Controllers\AppBaseController;
use App\Http\Requests\CreateNotaRequest;
use App\Http\Requests\UpdateNotaRequest;
use App\Models\Asignatura;
use App\Models\Estudiante;
use App\Models\Nota;
use App\Repositories\CentralRepository;
use App\Repositories\NotaRepository;
use Flash;
use Illuminate\Http\Request;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;
use Session;

class NotaController extends AppBaseController
{
    /** @var  NotaRepository */
    private $notaRepository;
    private $centralRepository;

    public function __construct(NotaRepository $notaRepo, CentralRepository $centralRepo)
    {
        $this->middleware('auth');
        $this->notaRepository = $notaRepo;
        $this->centralRepository = $centralRepo;
    }

    /**
     * Display a listing of the Nota.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->notaRepository->pushCriteria(new RequestCriteria($request));
        $notas = $this->notaRepository->orderBy('updated_at', 'desc')->paginate();

        return view('notas.index')
            ->with('notas', $notas);
    }

    public function indexByDocente(Asignatura $asignatura, Estudiante $estudiante)
    {
        $notas = Nota::where('asignatura_id', $asignatura->id)
        ->where('estudiante_id', $estudiante->id)
        ->orderBy('updated_at', 'desc')->paginate();

        return view('notas.indexByDocente')
            ->with([
                'notas'=> $notas
            ]);
    }

    /**
     * Commons Funtions Repository
     */
    public function sels()
    {
        $sels = [];
        $sels['periodo'] = $this->centralRepository->periodo;
        return $sels;
    }

    /**
     * Show the form for creating a new Nota.
     *
     * @return Response
     */
    public function create(Request $request)
    {
        $sels = $this->sels();

        return view('notas.create')->with(['sels' => $sels]);
    }

    public function createByDocente(Asignatura $asignatura, Estudiante $estudiante)
    {
        $sels = $this->sels();

        return view('notas.create')
        ->with([
            'sels' => $sels, 
            'estudiante' => $estudiante, 
            'asignatura' => $asignatura
        ]);
    }

    /**
     * Store a newly created Nota in storage.
     *
     * @param CreateNotaRequest $request
     *
     * @return Response
     */
    public function store(CreateNotaRequest $request)
    {
        $input = $request->all();
        $input['user_id'] = \Auth::id();

        $nota = $this->notaRepository->create($input);

        Session::flash('success', 'Nota registrado correctamente.');

        return redirect(route('notas.index'));
    }

    /**
     * Display the specified Nota.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $nota = $this->notaRepository->findWithoutFail($id);

        if (empty($nota)) {
            Session::flash('error', 'Nota No se encuentra registrado.');

            return redirect(route('notas.index'));
        }

        return view('notas.show')->with('nota', $nota);
    }

    /**
     * Show the form for editing the specified Nota.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $nota = $this->notaRepository->findWithoutFail($id);

        if (empty($nota)) {
            Session::flash('error', 'Nota No se encuentra registrado.');

            return redirect(route('notas.index'));
        }

         $sels = $this->sels();

        return view('notas.edit')->with(['nota' => $nota, 'sels' => $sels]);
    }

    /**
     * Update the specified Nota in storage.
     *
     * @param  int              $id
     * @param UpdateNotaRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateNotaRequest $request)
    {
        $nota = $this->notaRepository->findWithoutFail($id);

        if (empty($nota)) {
            Session::flash('error', 'Nota No se encuentra registrado.');

            return redirect(route('notas.index'));
        }

        $input = $request->all();
        $input['user_id'] = \Auth::id();

        $nota = $this->notaRepository->update($input, $id);

        Session::flash('success', 'Nota actualizado correctamente.');

        return redirect(route('notas.index'));
    }

    /**
     * Remove the specified Nota from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $nota = $this->notaRepository->findWithoutFail($id);

        if (empty($nota)) {
            Session::flash('error', 'Nota No se encuentra registrado.');

            return redirect(route('notas.index'));
        }

        $this->notaRepository->delete($id);

        Session::flash('success', 'Nota eliminado correctamente.');

        return redirect(route('notas.index'));
    }
}
