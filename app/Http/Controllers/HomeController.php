<?php

namespace App\Http\Controllers;
use App\Http\Requests;
use App\Models\Docente;
use App\Models\Asignatura;
use App\Repositories\CentralRepository;
use Auth;
use Carbon\Carbon;
use Flash;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    private $centralRepository;

    public function __construct(CentralRepository $centralRepo)
    {
        $this->middleware('auth');
        $this->centralRepository = $centralRepo;
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $docente = optional(auth()->user()->docente())->first();
       
       if ( ! is_null($docente) ) 
       {
          return view('home_docente')->with(['docente' => $docente]);
       }
        //dd($cont);
        return view('home');
    }

    public function asignaturas()
    {

        $docente = Docente::where('cedula', auth()->user()->cedula)->with('asignaturas')->first();
        $asignaturas = $docente->asignaturas;
        // dd($asignaturas);
        return view('asignaturas.index_docente')
        ->with(['docente' => $docente, 'asignaturas' => $asignaturas]);
    }

    public function estudiantesByAsignatura(Asignatura $asignatura)
    {
        $estudiantes = $asignatura->estudiantes;

        $docente = Docente::where('cedula', auth()->user()->cedula)->first();

        return view('estudiantes.index_estudiante')
        ->with(['docente' => $docente, 'estudiantes' => $estudiantes, 'asignatura' => $asignatura]);
    }



}
