<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateCursaRequest;
use App\Http\Requests\UpdateCursaRequest;
use App\Repositories\CentralRepository;
use App\Repositories\CursaRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;
use Session;

class CursaController extends AppBaseController
{
    /** @var  CursaRepository */
    private $cursaRepository;
    private $centralRepository;

    public function __construct(CursaRepository $cursaRepo, CentralRepository $centralRepo)
    {
        $this->middleware('auth');
        $this->middleware('roles:admin,supervisor');
        $this->cursaRepository = $cursaRepo;
        $this->centralRepository = $centralRepo;
    }

    /**
     * Display a listing of the Cursa.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->cursaRepository->pushCriteria(new RequestCriteria($request));
        $cursas = $this->cursaRepository->orderBy('updated_at', 'desc')->paginate();

        return view('cursas.index')
            ->with('cursas', $cursas);
    }
    /**
     * Commons Funtions Repository
     */
    public function sels()
    {
        $sels = [];
        $sels['estudiante_id'] = $this->centralRepository->estudiante_id();
        $sels['asignatura_id'] = $this->centralRepository->asignatura_id();
        return $sels;
    }

    /**
     * Show the form for creating a new Cursa.
     *
     * @return Response
     */
    public function create()
    {
        $sels = $this->sels();

        return view('cursas.create')->with(['sels' => $sels]);
    }

    /**
     * Store a newly created Cursa in storage.
     *
     * @param CreateCursaRequest $request
     *
     * @return Response
     */
    public function store(CreateCursaRequest $request)
    {
        $input = $request->all();
        $input['user_id'] = \Auth::id();

        $cursa = $this->cursaRepository->create($input);

        Session::flash('success', 'Cursa registrado correctamente.');

        return redirect(route('cursas.index'));
    }

    /**
     * Display the specified Cursa.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $cursa = $this->cursaRepository->findWithoutFail($id);

        if (empty($cursa)) {
            Session::flash('error', 'Cursa No se encuentra registrado.');

            return redirect(route('cursas.index'));
        }

        return view('cursas.show')->with('cursa', $cursa);
    }

    /**
     * Show the form for editing the specified Cursa.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $cursa = $this->cursaRepository->findWithoutFail($id);

        if (empty($cursa)) {
            Session::flash('error', 'Cursa No se encuentra registrado.');

            return redirect(route('cursas.index'));
        }

         $sels = $this->sels();

        return view('cursas.edit')->with(['cursa' => $cursa, 'sels' => $sels]);
    }

    /**
     * Update the specified Cursa in storage.
     *
     * @param  int              $id
     * @param UpdateCursaRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateCursaRequest $request)
    {
        $cursa = $this->cursaRepository->findWithoutFail($id);

        if (empty($cursa)) {
            Session::flash('error', 'Cursa No se encuentra registrado.');

            return redirect(route('cursas.index'));
        }

        $input = $request->all();
        $input['user_id'] = \Auth::id();

        $cursa = $this->cursaRepository->update($input, $id);

        Session::flash('success', 'Cursa actualizado correctamente.');

        return redirect(route('cursas.index'));
    }

    /**
     * Remove the specified Cursa from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $cursa = $this->cursaRepository->findWithoutFail($id);

        if (empty($cursa)) {
            Session::flash('error', 'Cursa No se encuentra registrado.');

            return redirect(route('cursas.index'));
        }

        $this->cursaRepository->delete($id);

        Session::flash('success', 'Cursa eliminado correctamente.');

        return redirect(route('cursas.index'));
    }
}
