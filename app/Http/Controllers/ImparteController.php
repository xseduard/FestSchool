<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateImparteRequest;
use App\Http\Requests\UpdateImparteRequest;
use App\Repositories\CentralRepository;
use App\Repositories\ImparteRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;
use Session;

class ImparteController extends AppBaseController
{
    /** @var  ImparteRepository */
    private $imparteRepository;
    private $centralRepository;

    public function __construct(ImparteRepository $imparteRepo, CentralRepository $centralRepo)
    {
        $this->middleware('auth');
        $this->middleware('roles:admin,supervisor');
        $this->imparteRepository = $imparteRepo;
        $this->centralRepository = $centralRepo;
    }

    /**
     * Display a listing of the Imparte.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->imparteRepository->pushCriteria(new RequestCriteria($request));
        $impartes = $this->imparteRepository->orderBy('updated_at', 'desc')->paginate();

        return view('impartes.index')
            ->with('impartes', $impartes);
    }
    /**
     * Commons Funtions Repository
     */
    public function sels()
    {
        $sels = [];
        $sels['docente_id'] = $this->centralRepository->docente_id();
        $sels['asignatura_id'] = $this->centralRepository->asignatura_id();
        return $sels;
    }

    /**
     * Show the form for creating a new Imparte.
     *
     * @return Response
     */
    public function create()
    {
        $sels = $this->sels();

        return view('impartes.create')->with(['sels' => $sels]);
    }

    /**
     * Store a newly created Imparte in storage.
     *
     * @param CreateImparteRequest $request
     *
     * @return Response
     */
    public function store(CreateImparteRequest $request)
    {
        $input = $request->all();
        $input['user_id'] = \Auth::id();

        $imparte = $this->imparteRepository->create($input);

        Session::flash('success', 'Imparte registrado correctamente.');

        return redirect(route('impartes.index'));
    }

    /**
     * Display the specified Imparte.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $imparte = $this->imparteRepository->findWithoutFail($id);

        if (empty($imparte)) {
            Session::flash('error', 'Imparte No se encuentra registrado.');

            return redirect(route('impartes.index'));
        }

        return view('impartes.show')->with('imparte', $imparte);
    }

    /**
     * Show the form for editing the specified Imparte.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $imparte = $this->imparteRepository->findWithoutFail($id);

        if (empty($imparte)) {
            Session::flash('error', 'Imparte No se encuentra registrado.');

            return redirect(route('impartes.index'));
        }

         $sels = $this->sels();

        return view('impartes.edit')->with(['imparte' => $imparte, 'sels' => $sels]);
    }

    /**
     * Update the specified Imparte in storage.
     *
     * @param  int              $id
     * @param UpdateImparteRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateImparteRequest $request)
    {
        $imparte = $this->imparteRepository->findWithoutFail($id);

        if (empty($imparte)) {
            Session::flash('error', 'Imparte No se encuentra registrado.');

            return redirect(route('impartes.index'));
        }

        $input = $request->all();
        $input['user_id'] = \Auth::id();

        $imparte = $this->imparteRepository->update($input, $id);

        Session::flash('success', 'Imparte actualizado correctamente.');

        return redirect(route('impartes.index'));
    }

    /**
     * Remove the specified Imparte from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $imparte = $this->imparteRepository->findWithoutFail($id);

        if (empty($imparte)) {
            Session::flash('error', 'Imparte No se encuentra registrado.');

            return redirect(route('impartes.index'));
        }

        $this->imparteRepository->delete($id);

        Session::flash('success', 'Imparte eliminado correctamente.');

        return redirect(route('impartes.index'));
    }
}
