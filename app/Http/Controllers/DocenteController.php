<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateDocenteRequest;
use App\Http\Requests\UpdateDocenteRequest;
use App\Repositories\CentralRepository;
use App\Repositories\DocenteRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;
use Session;

class DocenteController extends AppBaseController
{
    /** @var  DocenteRepository */
    private $docenteRepository;
    private $centralRepository;

    public function __construct(DocenteRepository $docenteRepo, CentralRepository $centralRepo)
    {
        $this->middleware('auth');
        $this->middleware('roles:admin,supervisor');
        $this->docenteRepository = $docenteRepo;
        $this->centralRepository = $centralRepo;
    }

    /**
     * Display a listing of the Docente.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->docenteRepository->pushCriteria(new RequestCriteria($request));
        $docentes = $this->docenteRepository->orderBy('updated_at', 'desc')->paginate();

        return view('docentes.index')
            ->with('docentes', $docentes);
    }
    /**
     * Commons Funtions Repository
     */
    public function sels()
    {
        $sels = [];
        $sels['municipio_id'] = $this->centralRepository->municipio_id();
        return $sels;
    }

    /**
     * Show the form for creating a new Docente.
     *
     * @return Response
     */
    public function create()
    {
        $sels = $this->sels();

        return view('docentes.create')->with(['sels' => $sels]);
    }

    /**
     * Store a newly created Docente in storage.
     *
     * @param CreateDocenteRequest $request
     *
     * @return Response
     */
    public function store(CreateDocenteRequest $request)
    {
        $input = $request->all();
        $input['user_id'] = \Auth::id();

        $docente = $this->docenteRepository->create($input);

        Session::flash('success', 'Docente registrado correctamente.');

        return redirect(route('docentes.index'));
    }

    /**
     * Display the specified Docente.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $docente = $this->docenteRepository->findWithoutFail($id);

        if (empty($docente)) {
            Session::flash('error', 'Docente No se encuentra registrado.');

            return redirect(route('docentes.index'));
        }

        return view('docentes.show')->with('docente', $docente);
    }

    /**
     * Show the form for editing the specified Docente.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $docente = $this->docenteRepository->findWithoutFail($id);

        if (empty($docente)) {
            Session::flash('error', 'Docente No se encuentra registrado.');

            return redirect(route('docentes.index'));
        }

         $sels = $this->sels();

        return view('docentes.edit')->with(['docente' => $docente, 'sels' => $sels]);
    }

    /**
     * Update the specified Docente in storage.
     *
     * @param  int              $id
     * @param UpdateDocenteRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateDocenteRequest $request)
    {
        $docente = $this->docenteRepository->findWithoutFail($id);

        if (empty($docente)) {
            Session::flash('error', 'Docente No se encuentra registrado.');

            return redirect(route('docentes.index'));
        }

        $input = $request->all();
        $input['user_id'] = \Auth::id();

        $docente = $this->docenteRepository->update($input, $id);

        Session::flash('success', 'Docente actualizado correctamente.');

        return redirect(route('docentes.index'));
    }

    /**
     * Remove the specified Docente from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $docente = $this->docenteRepository->findWithoutFail($id);

        if (empty($docente)) {
            Session::flash('error', 'Docente No se encuentra registrado.');

            return redirect(route('docentes.index'));
        }

        $this->docenteRepository->delete($id);

        Session::flash('success', 'Docente eliminado correctamente.');

        return redirect(route('docentes.index'));
    }
}
