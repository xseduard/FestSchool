<?php

namespace App\Http\Controllers;

use App\Http\Controllers\AppBaseController;
use App\Http\Requests\CreateEstudianteRequest;
use App\Http\Requests\InformeRequest;
use App\Http\Requests\UpdateEstudianteRequest;
use App\Models\Estudiante;
use App\Repositories\CentralRepository;
use App\Repositories\EstudianteRepository;
use Flash;
use Illuminate\Http\Request;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;
use Session;

class EstudianteController extends AppBaseController
{
    /** @var  EstudianteRepository */
    private $estudianteRepository;
    private $centralRepository;

    public function __construct(EstudianteRepository $estudianteRepo, CentralRepository $centralRepo)
    {
        $this->middleware('auth');
        $this->middleware('roles:admin,supervisor');
        $this->estudianteRepository = $estudianteRepo;
        $this->centralRepository = $centralRepo;
    }

    /**
     * Display a listing of the Estudiante.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->estudianteRepository->pushCriteria(new RequestCriteria($request));
        $estudiantes = $this->estudianteRepository->orderBy('updated_at', 'desc')->paginate();

        return view('estudiantes.index')
            ->with('estudiantes', $estudiantes);
    }
    /**
     * Commons Funtions Repository
     */
    public function sels()
    {
        $sels = [];
        $sels['grado'] = $this->centralRepository->grado;
        $sels['grupo'] = $this->centralRepository->grupo;
        $sels['periodo'] = $this->centralRepository->periodo;
        $sels['estudiante_id'] = $this->centralRepository->estudiante_id();
        $sels['municipio_id'] = $this->centralRepository->municipio_id();
        return $sels;
    }

    /**
     * Show the form for creating a new Estudiante.
     *
     * @return Response
     */
    public function create()
    {
        $sels = $this->sels();

        return view('estudiantes.create')->with(['sels' => $sels]);
    }

    /**
     * Store a newly created Estudiante in storage.
     *
     * @param CreateEstudianteRequest $request
     *
     * @return Response
     */
    public function store(CreateEstudianteRequest $request)
    {
        $input = $request->all();
        $input['user_id'] = \Auth::id();

        $estudiante = $this->estudianteRepository->create($input);

        Session::flash('success', 'Estudiante registrado correctamente.');

        return redirect(route('estudiantes.index'));
    }

    /**
     * Display the specified Estudiante.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $estudiante = $this->estudianteRepository->findWithoutFail($id);

        if (empty($estudiante)) {
            Session::flash('error', 'Estudiante No se encuentra registrado.');

            return redirect(route('estudiantes.index'));
        }

        return view('estudiantes.show')->with('estudiante', $estudiante);
    }

    /**
     * Show the form for editing the specified Estudiante.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $estudiante = $this->estudianteRepository->findWithoutFail($id);

        if (empty($estudiante)) {
            Session::flash('error', 'Estudiante No se encuentra registrado.');

            return redirect(route('estudiantes.index'));
        }

         $sels = $this->sels();

        return view('estudiantes.edit')->with(['estudiante' => $estudiante, 'sels' => $sels]);
    }

    /**
     * Update the specified Estudiante in storage.
     *
     * @param  int              $id
     * @param UpdateEstudianteRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateEstudianteRequest $request)
    {
        $estudiante = $this->estudianteRepository->findWithoutFail($id);

        if (empty($estudiante)) {
            Session::flash('error', 'Estudiante No se encuentra registrado.');

            return redirect(route('estudiantes.index'));
        }

        $input = $request->all();
        $input['user_id'] = \Auth::id();

        $estudiante = $this->estudianteRepository->update($input, $id);

        Session::flash('success', 'Estudiante actualizado correctamente.');

        return redirect(route('estudiantes.index'));
    }

    /**
     * Remove the specified Estudiante from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $estudiante = $this->estudianteRepository->findWithoutFail($id);

        if (empty($estudiante)) {
            Session::flash('error', 'Estudiante No se encuentra registrado.');

            return redirect(route('estudiantes.index'));
        }

        $this->estudianteRepository->delete($id);

        Session::flash('success', 'Estudiante eliminado correctamente.');

        return redirect(route('estudiantes.index'));
    }
    public function informe_crear(Request $request)
    {

         $sels = $this->sels();
         return view('estudiantes.informe_create')->with(['sels' => $sels]);
    }

    public function informe(InformeRequest $request)
    {
        $info = Estudiante::findOrFail($request->estudiante_id);
        // dd($info);
        $sels = $this->sels();
        
        return view('informe')->with(['info' => $info, 'sels' => $sels, 'periodo' => $request->periodo]);
    }
}
