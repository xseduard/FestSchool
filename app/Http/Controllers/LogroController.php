<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateLogroRequest;
use App\Http\Requests\UpdateLogroRequest;
use App\Repositories\CentralRepository;
use App\Repositories\LogroRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;
use Session;

class LogroController extends AppBaseController
{
    /** @var  LogroRepository */
    private $logroRepository;
    private $centralRepository;

    public function __construct(LogroRepository $logroRepo, CentralRepository $centralRepo)
    {
        $this->middleware('auth');
        $this->middleware('roles:admin,supervisor');
        $this->logroRepository = $logroRepo;
        $this->centralRepository = $centralRepo;
    }

    /**
     * Display a listing of the Logro.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->logroRepository->pushCriteria(new RequestCriteria($request));
        $logros = $this->logroRepository->orderBy('updated_at', 'desc')->paginate();

        return view('logros.index')
            ->with(['logros' => $logros, 'periodo' => $this->centralRepository->periodo]);
    }
    /**
     * Commons Funtions Repository
     */
    public function sels()
    {
        $sels = [];
        $sels['periodo'] = $this->centralRepository->periodo;
        $sels['asignatura_id'] = $this->centralRepository->asignatura_id();
        return $sels;
    }

    /**
     * Show the form for creating a new Logro.
     *
     * @return Response
     */
    public function create()
    {
        $sels = $this->sels();

        return view('logros.create')->with(['sels' => $sels]);
    }

    /**
     * Store a newly created Logro in storage.
     *
     * @param CreateLogroRequest $request
     *
     * @return Response
     */
    public function store(CreateLogroRequest $request)
    {
        $input = $request->all();
        $input['user_id'] = \Auth::id();

        $logro = $this->logroRepository->create($input);

        Session::flash('success', 'Logro registrado correctamente.');

        return redirect(route('logros.index'));
    }

    /**
     * Display the specified Logro.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $logro = $this->logroRepository->findWithoutFail($id);

        if (empty($logro)) {
            Session::flash('error', 'Logro No se encuentra registrado.');

            return redirect(route('logros.index'));
        }

        return view('logros.show')->with('logro', $logro);
    }

    /**
     * Show the form for editing the specified Logro.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $logro = $this->logroRepository->findWithoutFail($id);

        if (empty($logro)) {
            Session::flash('error', 'Logro No se encuentra registrado.');

            return redirect(route('logros.index'));
        }

         $sels = $this->sels();

        return view('logros.edit')->with(['logro' => $logro, 'sels' => $sels]);
    }

    /**
     * Update the specified Logro in storage.
     *
     * @param  int              $id
     * @param UpdateLogroRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateLogroRequest $request)
    {
        $logro = $this->logroRepository->findWithoutFail($id);

        if (empty($logro)) {
            Session::flash('error', 'Logro No se encuentra registrado.');

            return redirect(route('logros.index'));
        }

        $input = $request->all();
        $input['user_id'] = \Auth::id();

        $logro = $this->logroRepository->update($input, $id);

        Session::flash('success', 'Logro actualizado correctamente.');

        return redirect(route('logros.index'));
    }

    /**
     * Remove the specified Logro from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $logro = $this->logroRepository->findWithoutFail($id);

        if (empty($logro)) {
            Session::flash('error', 'Logro No se encuentra registrado.');

            return redirect(route('logros.index'));
        }

        $this->logroRepository->delete($id);

        Session::flash('success', 'Logro eliminado correctamente.');

        return redirect(route('logros.index'));
    }
}
