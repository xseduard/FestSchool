<?php

namespace App\Models;

use App\Traits\DatesTranslator;
use App\Traits\UserRelation;
use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Asignatura
 * @package App\Models
 * @version November 3, 2017, 11:37 am -05
 *
 * @method static Asignatura find($id=null, $columns = array())
 * @method static Asignatura|\Illuminate\Database\Eloquent\Collection findOrFail($id, $columns = ['*'])
 * @property string nombre
 * @property string Tipo_asignatura
 * @property string departamento
 */
class Asignatura extends Model
{
use DatesTranslator, UserRelation;
    use SoftDeletes;

    public $table = 'asignaturas';
    

    protected $dates = ['deleted_at'];


    public $fillable = [
        'nombre',
        'Tipo_asignatura',
        'departamento',
        'user_id'
    ];

    /**
     * The attributes that should be casted to native types.
     */
    protected $casts = [
        'nombre' => 'string',
        'Tipo_asignatura' => 'string',
        'departamento' => 'string',
        'user_id' => 'integer'
    ];

    /**
     * Validation rules
     */
    public static $rules = [
        'nombre' => 'required'
    ];

    public static $attributesCustom = [     
        
        'nombre',
        'Tipo_asignatura',
        'departamento',
           
    ];

    /**
     * Relationship Models
     */
    
    public function logros()
    {
        return $this->hasMany('App\Models\Logro');
    } 

    public function notas()
    {
        return $this->hasMany('App\Models\Nota');
    }  

    public function estudiantes()
    {
        return $this->belongsToMany('App\Models\Estudiante', 'cursas');
    }

    public function docentes()
    {
        return $this->belongsToMany('App\Models\Docente', 'impartes');
    }

    

    /**
     *  Ascensores & Mutadores
     */
}
