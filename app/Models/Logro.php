<?php

namespace App\Models;

use App\Traits\DatesTranslator;
use App\Traits\UserRelation;
use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Logro
 * @package App\Models
 * @version November 10, 2017, 12:25 am -05
 *
 * @method static Logro find($id=null, $columns = array())
 * @method static Logro|\Illuminate\Database\Eloquent\Collection findOrFail($id, $columns = ['*'])
 * @property integer asignatura_id
 * @property string periodo
 * @property string logro
 */
class Logro extends Model
{
use DatesTranslator, UserRelation;
    use SoftDeletes;

    public $table = 'logros';
    

    protected $dates = ['deleted_at'];


    public $fillable = [
        'asignatura_id',
        'periodo',
        'logro',
        'user_id'
    ];

    /**
     * The attributes that should be casted to native types.
     */
    protected $casts = [
        'asignatura_id' => 'integer',
        'periodo' => 'string',
        'logro' => 'string',
        'user_id' => 'integer'
    ];

    /**
     * Validation rules
     */
    public static $rules = [
        'asignatura_id' => 'required',
        'periodo' => 'required',
        'logro' => 'required'
    ];

    public static $attributesCustom = [     
        
        'asignatura_id',
        'periodo',
        'logro',
           
    ];

    /**
     * Relationship Models
     */
    
    // public function modelo(){
    //     return $this->belongsTo('App\Models\Modelo');
    // } 

    public function asignatura(){
        return $this->belongsTo('App\Models\Asignatura');
    }   

    

    /**
     *  Ascensores & Mutadores
     */
}
