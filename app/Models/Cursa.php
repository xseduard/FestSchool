<?php

namespace App\Models;

use App\Traits\DatesTranslator;
use App\Traits\UserRelation;
use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Cursa
 * @package App\Models
 * @version November 3, 2017, 1:04 pm -05
 *
 * @method static Cursa find($id=null, $columns = array())
 * @method static Cursa|\Illuminate\Database\Eloquent\Collection findOrFail($id, $columns = ['*'])
 * @property integer estudiante_id
 * @property integer asignatura_id
 * @property string ano
 */
class Cursa extends Model
{
use DatesTranslator, UserRelation;
    use SoftDeletes;

    public $table = 'cursas';
    

    protected $dates = ['deleted_at'];


    public $fillable = [
        'estudiante_id',
        'asignatura_id',
        'ano',
        'user_id'
    ];

    /**
     * The attributes that should be casted to native types.
     */
    protected $casts = [
        'estudiante_id' => 'integer',
        'asignatura_id' => 'integer',
        'ano' => 'string',
        'user_id' => 'integer'
    ];

    /**
     * Validation rules
     */
    public static $rules = [
        'estudiante_id' => 'required',
        'asignatura_id' => 'required',
        'ano' => 'required'
    ];

    public static $attributesCustom = [     
        
        'estudiante_id',
        'asignatura_id',
        'ano',
           
    ];

    /**
     * Relationship Models
     */
    
    public function estudiante(){
        return $this->belongsTo('App\Models\Estudiante');
    }  

    public function asignatura(){
        return $this->belongsTo('App\Models\Asignatura');
    }   

    

    /**
     *  Ascensores & Mutadores
     */
}
