<?php

namespace App\Models;

use App\Traits\DatesTranslator;
use App\Traits\UserRelation;
use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Sede
 * @package App\Models
 * @version September 29, 2017, 12:54 pm -05
 *
 * @method static Sede find($id=null, $columns = array())
 * @method static Sede|\Illuminate\Database\Eloquent\Collection findOrFail($id, $columns = ['*'])
 * @property string nombre_sede
 * @property string direccion
 * @property string director_sede
 * @property integer numero_sede
 * @property string observaciones
 */
class Sede extends Model
{
use DatesTranslator, UserRelation;
    use SoftDeletes;

    public $table = 'sedes';
    

    protected $dates = ['deleted_at'];


    public $fillable = [
        'nombre_sede',
        'direccion',
        'director_sede',
        'numero_sede',
        'observaciones',
        'user_id'
    ];

    /**
     * The attributes that should be casted to native types.
     */
    protected $casts = [
        'nombre_sede' => 'string',
        'direccion' => 'string',
        'director_sede' => 'string',
        'numero_sede' => 'integer',
        'observaciones' => 'string',
        'user_id' => 'integer'
    ];

    /**
     * Validation rules
     */
    public static $rules = [
        'nombre_sede' => 'required',
        'director_sede' => 'required',
        'numero_sede' => 'numeric'
    ];

    public static $attributesCustom = [     
        
        'nombre_sede',
        'direccion',
        'director_sede',
        'numero_sede',
        'observaciones',
           
    ];

    /**
     * Relationship Models
     */
    
    // public function modelo(){
    //     return $this->belongsTo('App\Models\Modelo');
    // }    

    

    /**
     *  Ascensores & Mutadores
     */
}
