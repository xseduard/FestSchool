<?php

namespace App\Models;

use App\Traits\DatesTranslator;
use App\Traits\UserRelation;
use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Nota
 * @package App\Models
 * @version November 10, 2017, 10:32 am -05
 *
 * @method static Nota find($id=null, $columns = array())
 * @method static Nota|\Illuminate\Database\Eloquent\Collection findOrFail($id, $columns = ['*'])
 * @property integer estudiante_id
 * @property integer asignatura_id
 * @property string periodo
 * @property string ano
 * @property string nota
 * @property string nota_2
 * @property string nota_3
 * @property string nota_4
 * @property string nota_5
 */
class Nota extends Model
{
use DatesTranslator, UserRelation;
    use SoftDeletes;

    public $table = 'notas';
    

    protected $dates = ['deleted_at'];


    public $fillable = [
        'estudiante_id',
        'asignatura_id',
        'periodo',
        'ano',
        'nota',
        'nota_2',
        'nota_3',
        'nota_4',
        'nota_5',
        'user_id'
    ];

    /**
     * The attributes that should be casted to native types.
     */
    protected $casts = [
        'estudiante_id' => 'integer',
        'asignatura_id' => 'integer',
        'periodo' => 'string',
        'ano' => 'string',
        'nota' => 'string',
        'nota_2' => 'string',
        'nota_3' => 'string',
        'nota_4' => 'string',
        'nota_5' => 'string',
        'user_id' => 'integer'
    ];

    /**
     * Validation rules
     */
    public static $rules = [
        'estudiante_id' => 'required',
        'asignatura_id' => 'required',
        'periodo' => 'required',
        'ano' => 'required',
        'nota' => 'required',
        'nota_5' => ''
    ];

    public static $attributesCustom = [     
        
        'estudiante_id',
        'asignatura_id',
        'periodo',
        'ano',
        'nota',
        'nota_2',
        'nota_3',
        'nota_4',
        'nota_5',
           
    ];

    /**
     * Relationship Models
     */
    
    public function estudiante(){
        return $this->belongsTo('App\Models\Estudiante');
    } 
    public function asignatura(){
        return $this->belongsTo('App\Models\Asignatura');
    }    

    

    /**
     *  Ascensores & Mutadores
     */

    public function getNotaFinalAttribute()
    {
         return ($this->nota + $this->nota_2 + $this->nota_3 + $this->nota_4 + $this->nota_5 )/ 5 ;
    }
}
