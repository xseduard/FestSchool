<?php

namespace App\Models;

use App\Traits\DatesTranslator;
use App\Traits\UserRelation;
use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Docente
 * @package App\Models
 * @version November 3, 2017, 11:27 am -05
 *
 * @method static Docente find($id=null, $columns = array())
 * @method static Docente|\Illuminate\Database\Eloquent\Collection findOrFail($id, $columns = ['*'])
 * @property string nombres
 * @property string apellidos
 * @property date fecha_nacimiento
 * @property string direccion
 * @property string municipio_id
 * @property string telefono
 * @property string celular
 * @property string afinadad
 * @property string Cedula
 */
class Docente extends Model
{
use DatesTranslator, UserRelation;
    use SoftDeletes;

    public $table = 'docentes';
    

    protected $dates = ['deleted_at'];


    public $fillable = [
        'nombres',
        'apellidos',
        'fecha_nacimiento',
        'direccion',
        'municipio_id',
        'telefono',
        'celular',
        'afinadad',
        'Cedula',
        'user_id'
    ];

    /**
     * The attributes that should be casted to native types.
     */
    protected $casts = [
        'nombres' => 'string',
        'apellidos' => 'string',
        'fecha_nacimiento' => 'date',
        'direccion' => 'string',
        'municipio_id' => 'string',
        'telefono' => 'string',
        'celular' => 'string',
        'afinadad' => 'string',
        'Cedula' => 'string',
        'user_id' => 'integer'
    ];

    /**
     * Validation rules
     */
    public static $rules = [
        'nombres' => 'required',
        'apellidos' => 'required',
        'fecha_nacimiento' => 'date',
        'municipio_id' => 'required',
        'Cedula' => 'required'
    ];

    public static $attributesCustom = [     
        
        'nombres',
        'apellidos',
        'fecha_nacimiento',
        'direccion',
        'municipio_id',
        'telefono',
        'celular',
        'afinadad',
        'Cedula',
           
    ];

    /**
     * Relationship Models
     */
    
    // public function modelo(){
    //     return $this->belongsTo('App\Models\Modelo');
    // }    

    public function asignaturas()
    {
        return $this->belongsToMany('App\Models\Asignatura', 'impartes');
    }  

    /**
     *  Ascensores & Mutadores
     */

    public function getFullNameAttribute()
    {
        return $this->nombres . " " . $this->apellidos;
    }
}
