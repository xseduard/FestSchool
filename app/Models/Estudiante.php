<?php

namespace App\Models;

use App\Traits\DatesTranslator;
use App\Traits\UserRelation;
use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Estudiante
 * @package App\Models
 * @version November 3, 2017, 10:48 am -05
 *
 * @method static Estudiante find($id=null, $columns = array())
 * @method static Estudiante|\Illuminate\Database\Eloquent\Collection findOrFail($id, $columns = ['*'])
 * @property string nombres
 * @property string apellidos
 * @property string di
 * @property string direccion
 * @property integer municipio_id
 * @property string nombree_acudiente
 * @property string apellidos_acudiente
 * @property string telefono_acudiente
 * @property string celular_acudiente
 * @property string grado
 * @property string grupo
 * @property integer edad
 * @property string observaciones
 * @property string tipo_rh
 */
class Estudiante extends Model
{
use DatesTranslator, UserRelation;
    use SoftDeletes;

    public $table = 'estudiantes';
    

    protected $dates = ['deleted_at'];


    public $fillable = [
        'nombres',
        'apellidos',
        'di',
        'direccion',
        'municipio_id',
        'nombree_acudiente',
        'apellidos_acudiente',
        'telefono_acudiente',
        'celular_acudiente',
        'grado',
        'grupo',
        'edad',
        'observaciones',
        'tipo_rh',
        'user_id'
    ];

    /**
     * The attributes that should be casted to native types.
     */
    protected $casts = [
        'nombres' => 'string',
        'apellidos' => 'string',
        'di' => 'string',
        'direccion' => 'string',
        'municipio_id' => 'integer',
        'nombree_acudiente' => 'string',
        'apellidos_acudiente' => 'string',
        'telefono_acudiente' => 'string',
        'celular_acudiente' => 'string',
        'grado' => 'string',
        'grupo' => 'string',
        'edad' => 'integer',
        'observaciones' => 'string',
        'tipo_rh' => 'string',
        'user_id' => 'integer'
    ];

    /**
     * Validation rules
     */
    public static $rules = [
        'nombres' => 'required',
        'apellidos' => 'required',
        'di' => 'numeric|required',
        'direccion' => 'required',
        'municipio_id' => 'required',
        'nombree_acudiente' => 'required',
        'apellidos_acudiente' => 'required',
        'telefono_acudiente' => 'required|numeric',
        'celular_acudiente' => 'numeric',
        'grado' => 'required',
        'grupo' => 'required',
        'edad' => 'numeric|required',
        'tipo_rh' => 'required'
    ];

    public static $attributesCustom = [     
        
           'nombres'             => 'nombres',
           'apellidos'           => 'apellidos',
           'di'                  => 'documento de identificación',
           'direccion'           => 'dirección',
           'municipio_id'        => 'ciudad / municipio',
           'nombree_acudiente'   => 'nombre del acudiente',
           'apellidos_acudiente' => 'apellidos acudiente',
           'telefono_acudiente'  => 'telefono del acudiente',
           'celular_acudiente'   => 'ceelular del acudiente',
           'grado'               => 'grado',
           'grupo'               => 'grupo',
           'edad'                => 'edad',
           'observaciones'       => 'observaciones',
           'tipo_rh'             => 'tipo de sangre (RH)',
           
    ];

    /**
     * Relationship Models
     */
    
    // public function modelo(){
    //     return $this->belongsTo('App\Models\Modelo');
    // }    

    

    /**
     *  Ascensores & Mutadores
     */

    public function getFullGradoAttribute()
    {
       return $this->grado . 'º-' . $this->grupo;
    }

    public function getFullNameAttribute()
    {
       return $this->nombres . ' ' . $this->apellidos;
    }

    public function asignaturas()
    {
        return $this->belongsToMany('App\Models\Asignatura', 'cursas');
    }
}
