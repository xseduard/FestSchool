<?php

namespace App\Models;

use App\Traits\DatesTranslator;
use App\Traits\UserRelation;
use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Imparte
 * @package App\Models
 * @version November 3, 2017, 2:14 pm -05
 *
 * @method static Imparte find($id=null, $columns = array())
 * @method static Imparte|\Illuminate\Database\Eloquent\Collection findOrFail($id, $columns = ['*'])
 * @property integer docente_id
 * @property integer asignatura_id
 * @property string ano
 */
class Imparte extends Model
{
use DatesTranslator, UserRelation;
    use SoftDeletes;

    public $table = 'impartes';
    

    protected $dates = ['deleted_at'];


    public $fillable = [
        'docente_id',
        'asignatura_id',
        'ano',
        'user_id'
    ];

    /**
     * The attributes that should be casted to native types.
     */
    protected $casts = [
        'docente_id' => 'integer',
        'asignatura_id' => 'integer',
        'ano' => 'string',
        'user_id' => 'integer'
    ];

    /**
     * Validation rules
     */
    public static $rules = [
        'docente_id' => 'required',
        'asignatura_id' => 'required',
        'ano' => 'required'
    ];

    public static $attributesCustom = [     
        
        'docente_id',
        'asignatura_id',
        'ano',
           
    ];

    /**
     * Relationship Models
     */
    
    public function docente(){
        return $this->belongsTo('App\Models\Docente');
    }  

    public function asignatura(){
        return $this->belongsTo('App\Models\Asignatura');
    } 

    /**
     *  Ascensores & Mutadores
     */
}
