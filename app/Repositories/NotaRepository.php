<?php

namespace App\Repositories;

use App\Models\Nota;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class NotaRepository
 * @package App\Repositories
 * @version November 10, 2017, 10:32 am -05
 *
 * @method Nota findWithoutFail($id, $columns = ['*'])
 * @method Nota find($id, $columns = ['*'])
 * @method Nota first($columns = ['*'])
*/
class NotaRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'estudiante_id',
        'asignatura_id',
        'periodo',
        'ano',
        'nota',
        'nota_2',
        'nota_3',
        'nota_4',
        'nota_5'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Nota::class;
    }
}
