<?php

namespace App\Repositories;

use App\Models\Imparte;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class ImparteRepository
 * @package App\Repositories
 * @version November 3, 2017, 2:14 pm -05
 *
 * @method Imparte findWithoutFail($id, $columns = ['*'])
 * @method Imparte find($id, $columns = ['*'])
 * @method Imparte first($columns = ['*'])
*/
class ImparteRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'docente_id',
        'asignatura_id',
        'ano'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Imparte::class;
    }
}
