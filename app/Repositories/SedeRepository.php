<?php

namespace App\Repositories;

use App\Models\Sede;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class SedeRepository
 * @package App\Repositories
 * @version September 29, 2017, 12:54 pm -05
 *
 * @method Sede findWithoutFail($id, $columns = ['*'])
 * @method Sede find($id, $columns = ['*'])
 * @method Sede first($columns = ['*'])
*/
class SedeRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'nombre_sede',
        'direccion',
        'director_sede',
        'numero_sede',
        'observaciones'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Sede::class;
    }
}
