<?php

namespace App\Repositories;

use App\Models\Cursa;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class CursaRepository
 * @package App\Repositories
 * @version November 3, 2017, 1:04 pm -05
 *
 * @method Cursa findWithoutFail($id, $columns = ['*'])
 * @method Cursa find($id, $columns = ['*'])
 * @method Cursa first($columns = ['*'])
*/
class CursaRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'estudiante_id',
        'asignatura_id',
        'ano'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Cursa::class;
    }
}
