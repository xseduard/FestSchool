<?php

namespace App\Repositories;

use App\Models\Estudiante;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class EstudianteRepository
 * @package App\Repositories
 * @version November 3, 2017, 10:48 am -05
 *
 * @method Estudiante findWithoutFail($id, $columns = ['*'])
 * @method Estudiante find($id, $columns = ['*'])
 * @method Estudiante first($columns = ['*'])
*/
class EstudianteRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'nombres',
        'apellidos',
        'di',
        'direccion',
        'municipio_id',
        'nombree_acudiente',
        'apellidos_acudiente',
        'telefono_acudiente',
        'celular_acudiente',
        'grado',
        'grupo',
        'edad',
        'observaciones',
        'tipo_rh'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Estudiante::class;
    }
}
