<?php

namespace App\Repositories;

use App\Models\Logro;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class LogroRepository
 * @package App\Repositories
 * @version November 10, 2017, 12:25 am -05
 *
 * @method Logro findWithoutFail($id, $columns = ['*'])
 * @method Logro find($id, $columns = ['*'])
 * @method Logro first($columns = ['*'])
*/
class LogroRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'asignatura_id',
        'periodo',
        'logro'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Logro::class;
    }
}
