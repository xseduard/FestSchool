<?php

namespace App\Repositories;


use App\Models\Asignatura;
use App\Models\Departamento;
use App\Models\Docente;
use App\Models\Estudiante;
use App\Models\Municipio;
use Carbon\Carbon;
use Flash;
use InfyOm\Generator\Common\BaseRepository;
use Jenssegers\Date\Date;

class CentralRepository extends BaseRepository
{
    public function model()
    {
        return Departamento::class;
    }
    

    public $generos = ['M' => 'Masculino', 'F' => 'Femenino', 'LGBTI' => 'LGBTI'];

    public $statusBool = ['0' => 'Activo', '1' => 'Inactivo'];

    public $grado = [ 1 => '1º', 2 => '2º', 3 => '3º', 4 => '4º', 5 => '5º', 6 => '6º', 7 => '7º', 8 => '8º', 9 => '9º', 10 => '10º', 11 => '11º', ];

	public $grupo = ['A' => 'A', 'B' => 'B', 'C' => 'C', 'D' => 'D', 'E' => 'E', ];

    public $periodo = ['p1' => 'Primer período', 'p2' => 'Segundo período', 'p3' => 'Tercer período', 'p4' => 'Cuarto período', ];

	public function municipio_id()
    {    
        $array = [];
            foreach ( Municipio::with('departamento')->get() as $key => $value ) 
            {                         
                $array[$value->id] = $value->nombre.", ".$value->departamento->nombre;                
            }
        return $array;
    }

    public function estudiante_id()
    {
            foreach (Estudiante::get()->toArray() as $key => $value) {
                
                $array[$value['id']]=$value['nombres']." ".$value['apellidos']." ".$value['grado']; 
                
            }
        return ($array);
    }

    public function docente_id()
    {
            foreach (Docente::get()->toArray() as $key => $value) {
                
                $array[$value['id']]=$value['nombres']." ".$value['apellidos']; 
                
            }
        return ($array);
    }


    public function asignatura_id()
    {
            foreach (Asignatura::get()->toArray() as $key => $value) {
                
                $array[$value['id']]=$value['nombre']; 
                
            }
        return ($array);
    }



}
