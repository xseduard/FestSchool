<?php

namespace App\Repositories;

use App\Models\Asignatura;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class AsignaturaRepository
 * @package App\Repositories
 * @version November 3, 2017, 11:37 am -05
 *
 * @method Asignatura findWithoutFail($id, $columns = ['*'])
 * @method Asignatura find($id, $columns = ['*'])
 * @method Asignatura first($columns = ['*'])
*/
class AsignaturaRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'nombre',
        'Tipo_asignatura',
        'departamento'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Asignatura::class;
    }
}
