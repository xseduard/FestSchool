<?php

namespace App\Repositories;

use App\Models\Docente;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class DocenteRepository
 * @package App\Repositories
 * @version November 3, 2017, 11:27 am -05
 *
 * @method Docente findWithoutFail($id, $columns = ['*'])
 * @method Docente find($id, $columns = ['*'])
 * @method Docente first($columns = ['*'])
*/
class DocenteRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'nombres',
        'apellidos',
        'fecha_nacimiento',
        'direccion',
        'municipio_id',
        'telefono',
        'celular',
        'afinadad',
        'Cedula'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Docente::class;
    }
}
