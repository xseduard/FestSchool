function sidebar_colapsado(){
  var ventana_ancho = $(window).width();
  var ventana_alto = $(window).height();
  if (ventana_ancho < 1369) {
    $("body").addClass("mini-navbar"); 
  } 
}

function getParameterByName(name) {
    name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
    var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
    results = regex.exec(location.search);
    return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
}

$('#top-search').val(getParameterByName('search'));
