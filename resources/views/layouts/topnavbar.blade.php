<div class="row border-bottom">
    <nav class="navbar navbar-static-top @yield('topnavbar-class')" role="navigation" style="margin-bottom: 0">
        <div class="navbar-header">
            <a class="navbar-minimalize minimalize-styl-2 btn btn-primary " href="#"><i class="fa fa-bars"></i> </a>
            @if (empty(Route::getCurrentRoute()->parameters))
                 {!!  Form::open(['url' => Route::getCurrentRoute()->uri, 'method' => 'GET', 'class' => 'navbar-form-custom', 'role' => 'search']) !!}                        
                    <div class="input-group navbar-form-custom">
                        <input type="text" placeholder="Buscar..." class="form-control" name="search" id="top-search" />
                    </div>
                {!! Form::close() !!} 
            @else
                {!!  Form::open(['url' => '#', 'class' => 'navbar-form-custom', 'role' => 'search']) !!}
                    <div class="input-group navbar-form-custom" >
                        <input type="text" data-toggle="tooltip" data-placement="bottom" title="Los filtros no esta disponibles en esta pagin" data-original-title="Los filtros no esta disponibles en esta pagina" name="search" class="form-control" placeholder="Buscar..." disabled />                  
                    </div>
                {!! Form::close() !!}   
            @endif
        </div>
        <ul class="nav navbar-top-links navbar-right">
            <li>
                <a href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();"> <i class="fa fa-sign-out"></i> Salir</a>

                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">{{ csrf_field() }}</form>
            </li>
        </ul>
    </nav>
</div>
