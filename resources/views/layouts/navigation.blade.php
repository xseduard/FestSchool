<nav class="navbar-default navbar-static-side" role="navigation">
    <div class="sidebar-collapse">
        <ul class="nav metismenu" id="side-menu">
            <li class="nav-header">
                <div class="dropdown profile-element">
                    <span>
                        <img alt="Avatar" class="img-circle" width="48px" src="{{ auth()->user()->avatar }}">
                    </span>
                    <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                        <span class="clear">
                            <span class="block m-t-xs">
                                <strong class="font-bold">{!! Auth::user()->nombres !!}</strong>
                            </span>
                            <span class="text-muted text-xs block"> 
                                {{  Auth::user()->roles->pluck('display_name')->implode(', ') }} 
                                <b class="caret"></b>
                            </span>
                        </span>
                    </a>
                    <ul class="dropdown-menu animated fadeInRight m-t-xs">
                        <li><a href="{{ route('usuarios.edit', auth()->id()) }}"> Perfil</a></li>
                        <li><a href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();"> Salir</a></li>
                    </ul>
                </div>
                <div class="logo-element">
                   FC
                </div>
            </li>
            @include('layouts.menu')
        </ul>

    </div>
</nav>
