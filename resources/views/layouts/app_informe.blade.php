<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
<html>
<head>
    <meta charset="utf-8">
    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
    <title>@yield('title') HarvardColege</title>


    <link rel="stylesheet" href="{!! asset('css/vendor.css') !!}" />
    <link rel="stylesheet" href="{!! asset('css/app.css') !!}" />
    {!! Html::style('/dependencia_local/bootstrap-xlgrid.min.css') !!}
    <link href="/src/css/select2/4.0.4/select2.min.css" rel="stylesheet" />
    {!! Html::style('/css/select2-lte.css') !!}
    <link rel="stylesheet" href="/src/css/datapicker/datepicker3.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/material-design-iconic-font/2.2.0/css/material-design-iconic-font.min.css">
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css">

     <!-- skin icheck -->
    @yield('css')
    @stack('css')
    <link rel="stylesheet" href="/css/main.css">

</head>
<body class="top-navigation" >

  <!-- Wrapper-->
    <div id="wrapper">

        <!-- Navigation -->
        {{-- @ include('layouts.navigation') --}}

        <!-- Page wraper -->
        <div id="page-wrapper" class="gray-bg">

            <!-- Page wrapper -->
            @include('layouts.menu_informe')

            <!-- Main view  -->
            @yield('content')

            <!-- Footer -->
            @include('layouts.footer')

        </div>
        <!-- End page wrapper-->

    </div>
    <!-- End wrapper-->

<script src="{!! asset('js/app.js') !!}" type="text/javascript"></script>
<script src="/src/js/select2/4.0.4/select2.min.js"></script>
<!-- datepicker  -->
    {!! HTML::script('/src/js/datapicker/bootstrap-datepicker.js') !!}
    <!-- datepicker español -->
    {!! HTML::script('/src/js/datapicker/locales/bootstrap-datepicker.es.js') !!}
    
<script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>
{!! HTML::script('/js/main.js') !!}


@section('scripts')
@show
@yield('scripts')
@stack('scripts')

@if ( config('app.env') == 'production' )
    @include('common.partial_chat')    
@endif
@include('common.alert-toastr')


</body>
</html>
