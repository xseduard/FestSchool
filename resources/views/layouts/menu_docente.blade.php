<div class="row border-bottom white-bg">
        <nav class="navbar navbar-static-top" role="navigation">
            <div class="navbar-header">
                <button aria-controls="navbar" aria-expanded="false" data-target="#navbar" data-toggle="collapse" class="navbar-toggle collapsed" type="button">
                    <i class="fa fa-reorder"></i>
                </button>
                <a href="#" class="navbar-brand">HarvardColege</a>
            </div>
            <div class="navbar-collapse collapse" id="navbar">
                <ul class="nav navbar-nav">
                    <li class="{{ Request::is('ver/asignaturas*') ? 'active' : '' }}">
                        <a aria-expanded="false" role="button" href="{{ route('ver.asignaturas') }}"> Ver Asignaturas</a>
                    </li>
                    <li class="{{ Request::is('ver/estudiantes*') ? 'active' : '' }}">
                        <a aria-expanded="false" role="button" href="{{ route('ver.estudiantes') }}"> Ver Estudiantes</a>
                    </li>

                    <li class="{{ Request::is('notas/indexByDocente*') ? 'active' : '' }}">
                        <a aria-expanded="false" role="button" href="{{ route('ver.estudiantes') }}"> Notas</a>
                    </li>

                    <li class="dropdown">
                        <a aria-expanded="false" role="button" href="#" class="dropdown-toggle" data-toggle="dropdown"> Calificaciones <span class="caret"></span></a>
                        <ul role="menu" class="dropdown-menu">
                            <li><a href="">Menu item</a></li>
                            <li><a href="">Menu item</a></li>
                            <li><a href="">Menu item</a></li>
                            <li><a href="">Menu item</a></li>
                        </ul>
                    </li>
           

                </ul>
                <ul class="nav navbar-top-links navbar-right">
                   <li>
                        <a href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();"> <i class="fa fa-sign-out"></i> Salir</a>

                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">{{ csrf_field() }}</form>
                    </li>
                </ul>
            </div>
        </nav>

    </div>
        