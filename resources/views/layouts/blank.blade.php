<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
<html>
<head>
    <meta charset="utf-8">
    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
    <title>@yield('title') FestCar</title>


    <link rel="stylesheet" href="{!! asset('css/vendor.css') !!}" />
    <link rel="stylesheet" href="{!! asset('css/app.css') !!}" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/material-design-iconic-font/2.2.0/css/material-design-iconic-font.min.css">
    {!! Html::style('/dependencia_local/bootstrap-xlgrid.min.css') !!}
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.4/css/select2.min.css" rel="stylesheet" />
    {!! Html::style('/css/select2-lte.css') !!}
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css">
     <!-- skin icheck -->
    {!! Html::style('/bower_components/admin-lte/plugins/iCheck/square/blue.css') !!}
    @yield('css')
    @stack('css')

</head>
<body style="background-color: #eee">

  <!-- Wrapper-->
    <div id="wrapper">

         <!-- Main view  -->
            @yield('content')

    </div>
    <!-- End wrapper-->

<script src="{!! asset('js/app.js') !!}" type="text/javascript"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.4/js/select2.min.js"></script>
<!-- datepicker  -->
    {!! HTML::script('/bower_components/admin-lte/plugins/datepicker/bootstrap-datepicker.js') !!}
    <!-- datepicker español -->
    {!! HTML::script('/bower_components/admin-lte/plugins/datepicker/locales/bootstrap-datepicker.es.js') !!}
{!! HTML::script('/bower_components/admin-lte/plugins/iCheck/icheck.min.js') !!}
{!! HTML::script('/js/main.js') !!}

@section('scripts')
@show
@yield('scripts')
@stack('scripts')

</body>
</html>
