<li class="{{ Request::is('home*') ? 'active' : '' }}">
    <a href="{!! url('/home') !!}"><i class="fa fa-home"></i><span class="nav-label">Inicio</span></a>
</li>
<li class="{{ Request::is('usuarios*') ? 'active' : '' }}">
    <a href="{!! route('usuarios.index') !!}" class="animsition-link"><i class="zmdi zmdi-accounts-add"></i><span class="nav-label">Cuentas de Usuario</span></a>
</li>

<li class="{{ Request::is('departamentos*') ? 'active' : '' }}">
    <a href="{!! route('departamentos.index') !!}"><span class="nav-label">Departamentos</span></a>
</li>
<li class="{{ Request::is('municipios*') ? 'active' : '' }}">
    <a href="{!! route('municipios.index') !!}"><span class="nav-label">Municipios</span></a>
</li>
<li class="{{ Request::is('roles*') ? 'active' : '' }}">
    <a href="{!! route('roles.index') !!}"><span class="nav-label">Roles</span></a>
</li>
<li class="{{ Request::is('sedes*') ? 'active' : '' }}">
    <a href="{!! route('sedes.index') !!}"><i class="fa fa-arrow-right"></i><span>Sedes</span></a>
</li>

<li class="{{ Request::is('estudiantes*') ? 'active' : '' }}">
    <a href="{!! route('estudiantes.index') !!}"><i class="fa fa-arrow-right"></i><span>Estudiantes</span></a>
</li>

<li class="{{ Request::is('docentes*') ? 'active' : '' }}">
    <a href="{!! route('docentes.index') !!}"><i class="fa fa-arrow-right"></i><span>Docentes</span></a>
</li>

<li class="{{ Request::is('asignaturas*') ? 'active' : '' }}">
    <a href="{!! route('asignaturas.index') !!}"><i class="fa fa-arrow-right"></i><span>Asignaturas</span></a>
</li>

<li class="{{ Request::is('cursas*') ? 'active' : '' }}">
    <a href="{!! route('cursas.index') !!}"><i class="fa fa-arrow-right"></i><span>Asignar Asignaturas (Estudiantes)</span></a>
</li>

<li class="{{ Request::is('impartes*') ? 'active' : '' }}">
    <a href="{!! route('impartes.index') !!}"><i class="fa fa-arrow-right"></i><span>Docente Imparte Asignatura</span></a>
</li>

<li class="{{ Request::is('logros*') ? 'active' : '' }}">
    <a href="{!! route('logros.index') !!}"><i class="fa fa-arrow-right"></i><span>Logros</span></a>
</li>

<li class="{{ Request::is('notas*') ? 'active' : '' }}">
    <a href="{!! route('notas.index') !!}"><i class="fa fa-edit"></i><span>Notas</span></a>
</li>

<li class="{{ Request::is('notas*') ? 'active' : '' }}">
    <a href="{!! route('informe.crear') !!}"><i class="fa fa-arrow-right"></i><span>Informe</span></a>
</li>

