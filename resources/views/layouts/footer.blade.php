<div class="footer">
    <div class="pull-right">
        <i class="fa fa-star"></i>
    </div>
    <div>
        <strong>Copyright © 2017</strong> <a href="https://transportedigital.com">HarvardColege</a> Technologies. All rights reserved.
    </div>
</div>
