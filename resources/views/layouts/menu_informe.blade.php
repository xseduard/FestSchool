<div class="row border-bottom white-bg">
        <nav class="navbar navbar-static-top" role="navigation">
            <div class="navbar-header">
                <button aria-controls="navbar" aria-expanded="false" data-target="#navbar" data-toggle="collapse" class="navbar-toggle collapsed" type="button" >
                    <i class="fa fa-reorder"></i>
                </button>
                <a href="{{ route('home') }}"" class="navbar-brand" style="background-color: #c2152d !important; color: black !important"><b>Harvard Colege</b></a>
            </div>
            <div class="navbar-collapse collapse" id="navbar">
                <ul class="nav navbar-nav">
                    <li class="#">
                        <a aria-expanded="false" role="button" href="{{ route('home') }}"> INFORME CALIFICACIONES</a>
                    </li>                 

                </ul>
                <ul class="nav navbar-top-links navbar-right">
                   <li>
                        <a href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();"> <i class="fa fa-sign-out"></i> Salir</a>

                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">{{ csrf_field() }}</form>
                    </li>
                </ul>
            </div>
        </nav>

    </div>
        