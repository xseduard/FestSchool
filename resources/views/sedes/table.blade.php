<table class="table table-responsive table-hover" id="sedes-table">
    <thead>
        <th>Nombre Sede</th>
        <th>Direccion</th>
        <th>Director Sede</th>
        <th>Numero Sede</th>
        <th>Observaciones</th>
        <th colspan="3" class="text-right">Acciones</th>
    </thead>
    <tbody>
    @foreach($sedes as $sede)
        <tr>
            <td>{!! $sede->nombre_sede !!}</td>
            <td>{!! $sede->direccion !!}</td>
            <td>{!! $sede->director_sede !!}</td>
            <td>{!! $sede->numero_sede !!}</td>
            <td>{!! $sede->observaciones !!}</td>
            <td>
                {!! Form::open(['route' => ['sedes.destroy', $sede->id], 'method' => 'delete']) !!}
                <div class='btn-group pull-right'>
                   {{--  <a href="{!! route('sedes.show', [$sede->id]) !!}" class='btn btn-default btn-sm'><i class="glyphicon glyphicon-eye-open"></i></a> --}}
                    <a href="{!! route('sedes.edit', [$sede->id]) !!}" class='btn btn-default btn-sm' title='Editar'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', [
                        'type' => 'submit',
                        'class' => 'btn btn-danger btn-sm',
                        'onclick' => "return confirm('¿Confirma que desea eliminar?')",
                        'title' => 'Eliminar'
                        ]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>