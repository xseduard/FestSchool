<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $sede->id !!}</p>
</div>

<!-- Nombre Sede Field -->
<div class="form-group">
    {!! Form::label('nombre_sede', 'Nombre Sede:') !!}
    <p>{!! $sede->nombre_sede !!}</p>
</div>

<!-- Direccion Field -->
<div class="form-group">
    {!! Form::label('direccion', 'Direccion:') !!}
    <p>{!! $sede->direccion !!}</p>
</div>

<!-- Director Sede Field -->
<div class="form-group">
    {!! Form::label('director_sede', 'Director Sede:') !!}
    <p>{!! $sede->director_sede !!}</p>
</div>

<!-- Numero Sede Field -->
<div class="form-group">
    {!! Form::label('numero_sede', 'Numero Sede:') !!}
    <p>{!! $sede->numero_sede !!}</p>
</div>

<!-- Observaciones Field -->
<div class="form-group">
    {!! Form::label('observaciones', 'Observaciones:') !!}
    <p>{!! $sede->observaciones !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{!! $sede->created_at !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{!! $sede->updated_at !!}</p>
</div>

