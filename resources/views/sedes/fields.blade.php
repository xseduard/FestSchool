<!-- Nombre Sede Field -->
<div class="form-group col-sm-6 col-lg-6 {{ $errors->has('nombre_sede') ? ' has-feedback has-error' : '' }}">
    {!! Form::label('nombre_sede', 'Nombre Sede') !!}
    {!! Form::text('nombre_sede', null, ['class' => 'form-control']) !!}
</div>

<!-- Direccion Field -->
<div class="form-group col-sm-6 col-lg-6 {{ $errors->has('direccion') ? ' has-feedback has-error' : '' }}">
    {!! Form::label('direccion', 'Direccion') !!}
    {!! Form::text('direccion', null, ['class' => 'form-control']) !!}
</div>

<!-- Director Sede Field -->
<div class="form-group col-sm-6 col-lg-6 {{ $errors->has('director_sede') ? ' has-feedback has-error' : '' }}">
    {!! Form::label('director_sede', 'Director Sede') !!}
    {!! Form::text('director_sede', null, ['class' => 'form-control']) !!}
</div>

<!-- Numero Sede Field -->
<div class="form-group col-sm-6 col-lg-6 {{ $errors->has('numero_sede') ? ' has-feedback has-error' : '' }}">
    {!! Form::label('numero_sede', 'Numero Sede') !!}
    {!! Form::text('numero_sede', null, ['class' => 'form-control']) !!}
</div>

<!-- Observaciones Field -->
<div class="form-group col-sm-12 col-lg-12 {{ $errors->has('observaciones') ? ' has-feedback has-error' : '' }}">
    {!! Form::label('observaciones', 'Observaciones') !!}
    {!! Form::textarea('observaciones', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::button('<i class="glyphicon glyphicon-floppy-disk"></i> Guardar', ['type' => 'submit', 'class' => 'btn btn-primary btn-flat']) !!}
    <a href="{!! route('sedes.index') !!}" class="btn btn-default btn-flat">Cancelar</a>
</div>