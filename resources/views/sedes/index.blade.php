@extends('layouts.app')

@section('title', 'Sedes |')

@section('content')
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-8">
            <h2>Sedes</h2>
            <ol class="breadcrumb">
                <li>
                    <a href="{{ route('home') }}">Inicio</a>
                </li>
                <li class="active">
                    <strong>Sedes</strong>
                </li>                        
            </ol>
        </div>
        <div class="col-lg-4">
            <div class="title-action">
                <a href="{!! route('sedes.create') !!}" class="btn btn-primary"><i class="fa fa-plus"></i> Agregar </a>
            </div>
        </div>
    </div>

    <div class="content">
        <div class="row wrapper wrapper-content animated fadeInRight">
            <div class="ibox">      
                @include('flash::message')
                <div class="ibox-content table-responsive">
                    @if($sedes->isEmpty())
                        <div class="alert alert-info alert-dismissible">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                            <h4><i class="icon fa fa-info"></i>Información</h4>
                           No se encontraron registros de Sedes en esta consulta.
                        </div>                    
                    @else                  
                        @include('sedes.table')
                        @include('common.paginate', ['records' => $sedes])
                    @endif
                </div>
            </div>
        </div>
    </div>
@endsection


