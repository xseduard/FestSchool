<table class="table table-responsive table-hover" id="asignaturas-table">
    <thead>
        <th>Nombre</th>
        <th>Tipo Asignatura</th>
        <th>Departamento</th>
        <th colspan="3" class="text-right">Acciones</th>
    </thead>
    <tbody>
    @foreach($asignaturas as $asignatura)
        <tr>
            <td>{!! $asignatura->nombre !!}</td>
            <td>{!! $asignatura->Tipo_asignatura !!}</td>
            <td>{!! $asignatura->departamento !!}</td>
            <td>
                {!! Form::open(['route' => ['asignaturas.destroy', $asignatura->id], 'method' => 'delete']) !!}
                <div class='btn-group pull-right'>
                   {{--  <a href="{!! route('asignaturas.show', [$asignatura->id]) !!}" class='btn btn-default btn-sm'><i class="glyphicon glyphicon-eye-open"></i></a> --}}
                    <a href="{!! route('asignaturas.edit', [$asignatura->id]) !!}" class='btn btn-default btn-sm' title='Editar'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', [
                        'type' => 'submit',
                        'class' => 'btn btn-danger btn-sm',
                        'onclick' => "return confirm('¿Confirma que desea eliminar?')",
                        'title' => 'Eliminar'
                        ]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>