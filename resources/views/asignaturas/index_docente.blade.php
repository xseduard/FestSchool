@extends('layouts.app_docente')

@section('title', 'Asignaturas |')

@section('content')
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-8">
            <h2>Asignaturas  asignadas a <b>{{ $docente->fullname }}</b></h2>
            <ol class="breadcrumb">
                <li>
                    <a href="{{ route('home') }}">Inicio</a>
                </li>
                <li class="active">
                    <strong>Asignaturas</strong>
                </li>                        
            </ol>
        </div>
    </div>

    <div class="content">
        <div class="row wrapper wrapper-content animated fadeInRight">
            <div class="ibox">      
                @include('flash::message')
                <div class="ibox-content table-responsive">
                    @if($asignaturas->isEmpty())
                        <div class="alert alert-info alert-dismissible">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                            <h4><i class="icon fa fa-info"></i>Información</h4>
                           No se encontraron registros de Asignaturas en esta consulta.
                        </div>                    
                    @else                  
                        @include('asignaturas.table_docente')
                        {{-- @ include('common.paginate', ['records' => $asignaturas]) --}}
                    @endif
                </div>
            </div>
        </div>
    </div>
@endsection


