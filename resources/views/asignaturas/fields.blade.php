<!-- Nombre Field -->
<div class="form-group col-sm-6 col-lg-6 {{ $errors->has('nombre') ? ' has-feedback has-error' : '' }}">
    {!! Form::label('nombre', 'Nombre') !!}
    {!! Form::text('nombre', null, ['class' => 'form-control']) !!}
</div>

<!-- Tipo Asignatura Field -->
<div class="form-group col-sm-6 col-lg-6 {{ $errors->has('Tipo_asignatura') ? ' has-feedback has-error' : '' }}">
    {!! Form::label('Tipo_asignatura', 'Tipo Asignatura') !!}
    {!! Form::text('Tipo_asignatura', null, ['class' => 'form-control']) !!}
</div>

<!-- Departamento Field -->
<div class="form-group col-sm-6 col-lg-6 {{ $errors->has('departamento') ? ' has-feedback has-error' : '' }}">
    {!! Form::label('departamento', 'Departamento') !!}
    {!! Form::text('departamento', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::button('<i class="glyphicon glyphicon-floppy-disk"></i> Guardar', ['type' => 'submit', 'class' => 'btn btn-primary btn-flat']) !!}
    <a href="{!! route('asignaturas.index') !!}" class="btn btn-default btn-flat">Cancelar</a>
</div>