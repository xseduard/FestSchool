<table class="table table-responsive table-hover" id="asignaturas-table">
    <thead>
        <th>Nombre</th>
        <th>Tipo Asignatura</th>
        <th>Departamento</th>
        <th colspan="3" class="text-right">Acciones</th>
    </thead>
    <tbody>
    @foreach($asignaturas as $asignatura)
        <tr>
            <td>{!! $asignatura->nombre !!}</td>
            <td>{!! $asignatura->Tipo_asignatura !!}</td>
            <td>{!! $asignatura->departamento !!}</td>
            <td>
               
                <div class='btn-group pull-right'>
                    <a href="{!! route('ver.estudiantesByAsignatura', [$asignatura->id]) !!}" class='btn btn-info btn-sm'><i class="fa fa-arrow-right"></i> Ver Estudiantes</a>
                </div>
            </td>
        </tr>
    @endforeach
    </tbody>
</table>