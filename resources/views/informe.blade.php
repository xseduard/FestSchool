@extends('layouts.app_informe')

@section('title', 'Inicio |')
@section('topnavbar-class', 'white-bg')

@section('content')
{{--     <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-8">
            <h2>FestCar Home</h2>
            <ol class="breadcrumb">
                <li>
                  <strong>Inicio</strong>
                </li>                  
            </ol>
        </div>        
    </div> --}}
    <div class="content">
            @include('flash::message')    
            @include('common.errors')
            <div class="clearfix"></div>
           <!-- contenido -->
     
       
      <div class="row">
        <div class="col-sm-12">
              <center><img style="margin-top: 30px; margin-bottom: 30px; height: 180px" src="{{ asset('/multimedia/web/logo-white-v2.png') }}"> </center>
          </div>

          <div class="col-sm-offset-2 col-sm-8">
           <div class="alert alert-danger">      
                  @include('flash::message')
                  
                      <h3>Informe de calificaciones No. 
                        <b>
                        @php
                          $ramdom = rand(); 
                          echo $ramdom;

                        @endphp
                        </b> 
                        Periodo 
                        <b>{{ $sels['periodo'][$periodo] }}</b>
                    </h4>
                  
              </div>
         </div>

         <div class="col-sm-offset-2 col-md-8">
            <!-- LINE CHART -->
            <div style="margin-top: 30px">
                <div class="col-sm-4">
                  <img src="/fondo/student.png" style="width: 150px" alt="estudiante">
                </div>
                <div class="col-sm-8">
                  <h1 style="font-size: 54px">{{ $info->fullname }}</h1>
                </div>
                <div class="col-sm-12"  style="font-size: 24px">                  
                  <p>Sistema de información para la gestion y control de instituciones educativas.</p>               
                </div>                
            </div>
            <!-- /.box -->
          </div>
          
          
       </div>
       <div class="row">
        <div class="col-sm-offset-2 col-sm-8">
         <div class="ibox">      
                @include('flash::message')
                <div class="ibox-content table-responsive">
    <h2>Datos del estudiante</h2>
  <table class="table table-responsive table-bordered" >
    <thead>
        <th>TI</th>
        <th>Dirección</th>
        <th>Grado</th>
        <th>Grupo</th>
        <th>Edad</th>
        <th>Tipo Rh</th>
    </thead>
    <tbody>
             <td>{{ $info->di }}</td>
                    <td>{{ $info->direccion }}</td>
                    <td>{{ $info->grado }}</td>
                    <td>{{ $info->grupo }}</td>
                    <td>{{ $info->edad }}</td>
                    <td>{{ $info->tipo_rh }}</td> 
    </tbody>     
    </table>
    <h2>Datos del Acudiente</h2>
    <table class="table table-responsive table-bordered" >
    <thead>
        <th>Nombre</th>
        <th>Apellido</th>
        <th>Teléfono</th>
        <th>Celular</th>
    </thead>
    <tbody>
      <td>{{ $info->nombree_acudiente }}</td>
                    <td>{{ $info->apellidos_acudiente }}</td>
                    <td>{{ $info->telefono_acudiente }}</td>
                    <td>{{ $info->celular_acudiente }}</td>
      </tbody>
    </table>                   
                 

      <hr>
     <h1>Asignaturas</h1>
    @foreach ($info->asignaturas as $asignatura)
  <table class="table table-responsive  table-hover table-bordered" >
    <thead>
        <th>Asignatura</th>
        <th>Docente</th>
        <th>Notas</th>
    </thead>
    <tbody>
  <td>
      <h3>{{ $asignatura->nombre }}</h3> <br>
      <h4>Logros</h4>
      @foreach ($asignatura->logros as $logro)
        {{ $logro->logro }}
      @endforeach
  </td>
  <td>
     {{ $asignatura->docentes->first()->fullname }}
  </td>
  <td>
     <table class="table table-responsive  table-hover table-bordered" >
    @foreach ($asignatura->notas as $nota)

      <tr>
        <td>{{ $nota->nota }} </td>
      <td>{{ $nota->nota_2 }} </td>
      <td>{{ $nota->nota_3 }} </td>
      <td>{{ $nota->nota_4 }} </td>
      <td>{{ $nota->nota_5 }}</td>
      <th style="background-color: {{ $nota->notafinal >= 3 ? 'green' : 'red'}}; color: white">{{ $nota->notafinal }}</th>
      </tr>      
      
    
    @endforeach
  </table>
  </td>
      
    </tbody>
  </table>
    @endforeach                    
                </div>
            </div>
       </div>

       <div class="ibox">
         hola
       </div>
      </div>
    </div>

@endsection

@push('css')
  <link href="/src/css/bootstrapTour/bootstrap-tour.min.css" rel="stylesheet">
@endpush

@section('scripts')
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.6.0/Chart.bundle.min.js"></script>
    <script src="/src/js/bootstrapTour/bootstrap-tour.min.js"></script>
   
    
@endsection
