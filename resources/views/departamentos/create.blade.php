@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Departamento
        </h1>
    </section>
    <div class="content">
        @include('adminlte-templates::common.errors')
        <div class="ibox">

            <div class="ibox-content">
                <div class="row">
                    {!! Form::open(['route' => 'departamentos.store']) !!}  

                        @include('departamentos.fields')

                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@endsection
