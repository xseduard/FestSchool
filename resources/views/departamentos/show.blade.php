@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Departamento
        </h1>
    </section>
    <div class="content">
        <div class="ibox">
            <div class="ibox-content">
                <div class="row" style="padding-left: 20px">
                    @include('departamentos.show_fields')
                    <a href="{!! route('departamentos.index') !!}" class="btn btn-default">Back</a>
                </div>
            </div>
        </div>
    </div>
@endsection
