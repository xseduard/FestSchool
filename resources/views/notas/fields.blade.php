
    {!! Form::hidden('estudiante_id', $estudiante->id, ['class' => 'form-control']) !!}

    {!! Form::hidden('asignatura_id', $asignatura->id, ['class' => 'form-control']) !!}


<!-- Periodo Selector -->
<div class="form-group col-sm-4 col-lg-4 {{ $errors->has('periodo') ? ' has-feedback has-error' : '' }}">
    {!! Form::label('periodo', 'Periodo') !!}
    {!! Form::select('periodo', $sels['periodo'], null, ['class' => 'form-control select2', 'style' => 'width: 100%', 'placeholder'=>'Seleccione...*']) !!}
</div>

<!-- Ano Selector -->
<div class="form-group col-sm-4 col-lg-4 {{ $errors->has('ano') ? ' has-feedback has-error' : '' }}">
    {!! Form::label('ano', 'Año') !!}
     {!! Form::text('ano', 2017, ['class' => 'form-control', 'readonly']) !!}
</div>

<!-- Nota Field -->
<div class="form-group col-sm-4 col-lg-4 {{ $errors->has('nota') ? ' has-feedback has-error' : '' }}">
    {!! Form::label('nota', 'Nota 1') !!}
    {!! Form::text('nota', null, ['class' => 'form-control']) !!}
</div>

<!-- Nota 2 Field -->
<div class="form-group col-sm-4 col-lg-4 {{ $errors->has('nota_2') ? ' has-feedback has-error' : '' }}">
    {!! Form::label('nota_2', 'Nota 2') !!}
    {!! Form::text('nota_2', null, ['class' => 'form-control']) !!}
</div>

<!-- Nota 3 Field -->
<div class="form-group col-sm-4 col-lg-4 {{ $errors->has('nota_3') ? ' has-feedback has-error' : '' }}">
    {!! Form::label('nota_3', 'Nota 3') !!}
    {!! Form::text('nota_3', null, ['class' => 'form-control']) !!}
</div>

<!-- Nota 4 Field -->
<div class="form-group col-sm-4 col-lg-4 {{ $errors->has('nota_4') ? ' has-feedback has-error' : '' }}">
    {!! Form::label('nota_4', 'Nota 4') !!}
    {!! Form::text('nota_4', null, ['class' => 'form-control']) !!}
</div>

<!-- Nota 5 Field -->
<div class="form-group col-sm-4 col-lg-4 {{ $errors->has('nota_5') ? ' has-feedback has-error' : '' }}">
    {!! Form::label('nota_5', 'Nota 5') !!}
    {!! Form::text('nota_5', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::button('<i class="glyphicon glyphicon-floppy-disk"></i> Guardar', ['type' => 'submit', 'class' => 'btn btn-primary btn-flat']) !!}
    <a href="{!! route('notas.index') !!}" class="btn btn-default btn-flat">Cancelar</a>
</div>