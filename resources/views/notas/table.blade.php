<table class="table table-responsive table-hover" id="notas-table">
    <thead>
        <th>Estudiante</th>
        <th>Asignatura</th>
        <th>Periodo</th>
        <th>Año</th>
        <th>Nota 1</th>
        <th>Nota 2</th>
        <th>Nota 3</th>
        <th>Nota 4</th>
        <th>Nota 5</th>
        <th>Nota Final</th>
        <th colspan="3" class="text-right">Acciones</th>
    </thead>
    <tbody>
    @foreach($notas as $nota)
        <tr>
            <td>{!! $nota->estudiante->fullname !!}</td>
            <td>{!! $nota->asignatura->nombre !!}</td>
            <td>{!! $nota->periodo !!}</td>
            <td>{!! $nota->ano !!}</td>
            <td><span class="label label-defaut">{!! $nota->nota !!}</span></td>
            <td><span class="label label-defaut">{!! $nota->nota_2 !!}</span></td>
            <td><span class="label label-defaut">{!! $nota->nota_3 !!}</span></td>
            <td><span class="label label-defaut">{!! $nota->nota_4 !!}</span></td>
            <td><span class="label label-defaut">{!! $nota->nota_5 !!}</span></td>
            <td>
                @if ( $nota->notafinal >= 3 )
                    <span class="label label-primary">{!! $nota->notafinal !!}</span>
                @else
                    <span class="label label-danger">{!! $nota->notafinal !!}</span>
                @endif
            </td>
            <td>
                {!! Form::open(['route' => ['notas.destroy', $nota->id], 'method' => 'delete']) !!}
                <div class='btn-group pull-right'>
                   {{--  <a href="{!! route('notas.show', [$nota->id]) !!}" class='btn btn-default btn-sm'><i class="glyphicon glyphicon-eye-open"></i></a> --}}
                    <a href="{!! route('notas.edit', [$nota->id]) !!}" class='btn btn-default btn-sm' title='Editar'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', [
                        'type' => 'submit',
                        'class' => 'btn btn-danger btn-sm',
                        'onclick' => "return confirm('¿Confirma que desea eliminar?')",
                        'title' => 'Eliminar'
                        ]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>