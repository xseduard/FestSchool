<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $nota->id !!}</p>
</div>

<!-- Estudiante Id Field -->
<div class="form-group">
    {!! Form::label('estudiante_id', 'Estudiante Id:') !!}
    <p>{!! $nota->estudiante_id !!}</p>
</div>

<!-- Asignatura Id Field -->
<div class="form-group">
    {!! Form::label('asignatura_id', 'Asignatura Id:') !!}
    <p>{!! $nota->asignatura_id !!}</p>
</div>

<!-- Periodo Field -->
<div class="form-group">
    {!! Form::label('periodo', 'Periodo:') !!}
    <p>{!! $nota->periodo !!}</p>
</div>

<!-- Ano Field -->
<div class="form-group">
    {!! Form::label('ano', 'Ano:') !!}
    <p>{!! $nota->ano !!}</p>
</div>

<!-- Nota Field -->
<div class="form-group">
    {!! Form::label('nota', 'Nota:') !!}
    <p>{!! $nota->nota !!}</p>
</div>

<!-- Nota 2 Field -->
<div class="form-group">
    {!! Form::label('nota_2', 'Nota 2:') !!}
    <p>{!! $nota->nota_2 !!}</p>
</div>

<!-- Nota 3 Field -->
<div class="form-group">
    {!! Form::label('nota_3', 'Nota 3:') !!}
    <p>{!! $nota->nota_3 !!}</p>
</div>

<!-- Nota 4 Field -->
<div class="form-group">
    {!! Form::label('nota_4', 'Nota 4:') !!}
    <p>{!! $nota->nota_4 !!}</p>
</div>

<!-- Nota 5 Field -->
<div class="form-group">
    {!! Form::label('nota_5', 'Nota 5:') !!}
    <p>{!! $nota->nota_5 !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{!! $nota->created_at !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{!! $nota->updated_at !!}</p>
</div>

