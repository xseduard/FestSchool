@extends('layouts.app_docente')

@section('title', 'Notas |')

@section('content')
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-8">
            <h2>Notas</h2>
            <ol class="breadcrumb">
                <li>
                    <a href="{{ route('home') }}">Inicio</a>
                </li>
                <li class="active">
                    <strong>Notas</strong>
                </li>                        
            </ol>
        </div>
    </div>

    <div class="content">
        <div class="row wrapper wrapper-content animated fadeInRight">
            <div class="ibox">      
                @include('flash::message')
                <div class="ibox-content table-responsive">
                    @if($notas->isEmpty())
                        <div class="alert alert-info alert-dismissible">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                            <h4><i class="icon fa fa-info"></i>Información</h4>
                           No se encontraron registros de Notas en esta consulta.
                        </div>                    
                    @else                  
                        @include('notas.table')
                        @include('common.paginate', ['records' => $notas])
                    @endif
                </div>
            </div>
        </div>
    </div>
@endsection


