@extends('layouts.app_docente')

@section('title', 'Estudiantes |')

@section('content')
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-8">
            <h2>Estudiantes en la asignatura <b>{{ $asignatura->nombre }}</b></h2>
            <ol class="breadcrumb">
                <li>
                    <a href="{{ route('home') }}">Inicio</a>
                </li>
                <li class="active">
                    <strong>Estudiantes</strong>
                </li>                        
            </ol>
        </div>
    </div>

    <div class="content">
        <div class="row wrapper wrapper-content animated fadeInRight">
            <div class="ibox">      
                @include('flash::message')
                <div class="ibox-content table-responsive">
                    @if($estudiantes->isEmpty())
                        <div class="alert alert-info alert-dismissible">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                            <h4><i class="icon fa fa-info"></i>Información</h4>
                           No se encontraron registros de Estudiantes en esta consulta.
                        </div>                    
                    @else                  
                        @include('estudiantes.table_estudiante')
                        {{-- @include('common.paginate', ['records' => $estudiantes]) --}}
                    @endif
                </div>
            </div>
        </div>
    </div>
@endsection


