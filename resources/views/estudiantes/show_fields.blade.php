<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $estudiante->id !!}</p>
</div>

<!-- Nombres Field -->
<div class="form-group">
    {!! Form::label('nombres', 'Nombres:') !!}
    <p>{!! $estudiante->nombres !!}</p>
</div>

<!-- Apellidos Field -->
<div class="form-group">
    {!! Form::label('apellidos', 'Apellidos:') !!}
    <p>{!! $estudiante->apellidos !!}</p>
</div>

<!-- Di Field -->
<div class="form-group">
    {!! Form::label('di', 'Di:') !!}
    <p>{!! $estudiante->di !!}</p>
</div>

<!-- Direccion Field -->
<div class="form-group">
    {!! Form::label('direccion', 'Direccion:') !!}
    <p>{!! $estudiante->direccion !!}</p>
</div>

<!-- Municipio Id Field -->
<div class="form-group">
    {!! Form::label('municipio_id', 'Municipio Id:') !!}
    <p>{!! $estudiante->municipio_id !!}</p>
</div>

<!-- Nombree Acudiente Field -->
<div class="form-group">
    {!! Form::label('nombree_acudiente', 'Nombree Acudiente:') !!}
    <p>{!! $estudiante->nombree_acudiente !!}</p>
</div>

<!-- Apellidos Acudiente Field -->
<div class="form-group">
    {!! Form::label('apellidos_acudiente', 'Apellidos Acudiente:') !!}
    <p>{!! $estudiante->apellidos_acudiente !!}</p>
</div>

<!-- Telefono Acudiente Field -->
<div class="form-group">
    {!! Form::label('telefono_acudiente', 'Telefono Acudiente:') !!}
    <p>{!! $estudiante->telefono_acudiente !!}</p>
</div>

<!-- Celular Acudiente Field -->
<div class="form-group">
    {!! Form::label('celular_acudiente', 'Celular Acudiente:') !!}
    <p>{!! $estudiante->celular_acudiente !!}</p>
</div>

<!-- Grado Field -->
<div class="form-group">
    {!! Form::label('grado', 'Grado:') !!}
    <p>{!! $estudiante->grado !!}</p>
</div>

<!-- Grupo Field -->
<div class="form-group">
    {!! Form::label('grupo', 'Grupo:') !!}
    <p>{!! $estudiante->grupo !!}</p>
</div>

<!-- Edad Field -->
<div class="form-group">
    {!! Form::label('edad', 'Edad:') !!}
    <p>{!! $estudiante->edad !!}</p>
</div>

<!-- Observaciones Field -->
<div class="form-group">
    {!! Form::label('observaciones', 'Observaciones:') !!}
    <p>{!! $estudiante->observaciones !!}</p>
</div>

<!-- Tipo Rh Field -->
<div class="form-group">
    {!! Form::label('tipo_rh', 'Tipo Rh:') !!}
    <p>{!! $estudiante->tipo_rh !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{!! $estudiante->created_at !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{!! $estudiante->updated_at !!}</p>
</div>

