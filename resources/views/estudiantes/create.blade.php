@extends('layouts.app')

@section('title', 'Estudiantes |')

@section('content')
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-8">
            <h2>Estudiantes</h2>
            <ol class="breadcrumb">
                <li>
                    <a href="{{ route('home') }}">Inicio</a>
                </li>
                <li>
                    <a href="{!! route('estudiantes.index') !!}">Estudiantes</a>
                </li>
                <li class="active">
                    <strong>Registrar</strong>
                </li>                      
            </ol>
        </div>
    </div>
    <div class="content">
        <div class="row wrapper wrapper-content animated fadeInRight">
            <div class="ibox">
                @include('common.errors')
                <div class="ibox-content">
                    <div class="row">
                        {!! Form::open(['route' => 'estudiantes.store']) !!}

                            @include('estudiantes.fields')

                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@include('common.partial-select-datepicker')