<!-- Nombres Field -->
<div class="form-group col-sm-6 col-lg-6 {{ $errors->has('nombres') ? ' has-feedback has-error' : '' }}">
    {!! Form::label('nombres', 'Nombres') !!}
    {!! Form::text('nombres', null, ['class' => 'form-control']) !!}
</div>

<!-- Apellidos Field -->
<div class="form-group col-sm-6 col-lg-6 {{ $errors->has('apellidos') ? ' has-feedback has-error' : '' }}">
    {!! Form::label('apellidos', 'Apellidos') !!}
    {!! Form::text('apellidos', null, ['class' => 'form-control']) !!}
</div>

<!-- Di Field -->
<div class="form-group col-sm-6 col-lg-6 {{ $errors->has('di') ? ' has-feedback has-error' : '' }}">
    {!! Form::label('di', 'Docuemnto de identificación') !!}
    {!! Form::text('di', null, ['class' => 'form-control']) !!}
</div>

<!-- Direccion Field -->
<div class="form-group col-sm-6 col-lg-6 {{ $errors->has('direccion') ? ' has-feedback has-error' : '' }}">
    {!! Form::label('direccion', 'Direccion') !!}
    {!! Form::text('direccion', null, ['class' => 'form-control']) !!}
</div>

<!-- Municipio Id Selector -->
<div class="form-group col-sm-6 col-lg-6 {{ $errors->has('municipio_id') ? ' has-feedback has-error' : '' }}">
    {!! Form::label('municipio_id', 'Municipio') !!}
    {!! Form::select('municipio_id', $sels['municipio_id'], null, ['class' => 'form-control select2', 'style' => 'width: 100%', 'placeholder'=>'Seleccione...*']) !!}
</div>

<!-- Nombree Acudiente Field -->
<div class="form-group col-sm-6 col-lg-6 {{ $errors->has('nombree_acudiente') ? ' has-feedback has-error' : '' }}">
    {!! Form::label('nombree_acudiente', 'Nombre del Acudiente') !!}
    {!! Form::text('nombree_acudiente', null, ['class' => 'form-control']) !!}
</div>

<!-- Apellidos Acudiente Field -->
<div class="form-group col-sm-6 col-lg-6 {{ $errors->has('apellidos_acudiente') ? ' has-feedback has-error' : '' }}">
    {!! Form::label('apellidos_acudiente', 'Apellidos del Acudiente') !!}
    {!! Form::text('apellidos_acudiente', null, ['class' => 'form-control']) !!}
</div>

<!-- Telefono Acudiente Field -->
<div class="form-group col-sm-6 col-lg-6 {{ $errors->has('telefono_acudiente') ? ' has-feedback has-error' : '' }}">
    {!! Form::label('telefono_acudiente', 'Telefono del Acudiente') !!}
    {!! Form::text('telefono_acudiente', null, ['class' => 'form-control']) !!}
</div>

<!-- Celular Acudiente Field -->
<div class="form-group col-sm-6 col-lg-6 {{ $errors->has('celular_acudiente') ? ' has-feedback has-error' : '' }}">
    {!! Form::label('celular_acudiente', 'Celular del Acudiente') !!}
    {!! Form::text('celular_acudiente', null, ['class' => 'form-control']) !!}
</div>

<!-- Grado Selector -->
<div class="form-group col-sm-3 col-lg-3 {{ $errors->has('grado') ? ' has-feedback has-error' : '' }}">
    {!! Form::label('grado', 'Grado') !!}
    {!! Form::select('grado', $sels['grado'], null, ['class' => 'form-control select2', 'style' => 'width: 100%', 'placeholder'=>'Seleccione...*']) !!}
</div>

<!-- Grupo Selector -->
<div class="form-group col-sm-3 col-lg-3 {{ $errors->has('grupo') ? ' has-feedback has-error' : '' }}">
    {!! Form::label('grupo', 'Grupo') !!}
    {!! Form::select('grupo', $sels['grupo'], null, ['class' => 'form-control select2', 'style' => 'width: 100%', 'placeholder'=>'Seleccione...*']) !!}
</div>

<!-- Edad Field -->
<div class="form-group col-sm-6 col-lg-6 {{ $errors->has('edad') ? ' has-feedback has-error' : '' }}">
    {!! Form::label('edad', 'Edad') !!}
    {!! Form::number('edad', null, ['class' => 'form-control']) !!}
</div>


<!-- Tipo Rh Field -->
<div class="form-group col-sm-6 col-lg-6 {{ $errors->has('tipo_rh') ? ' has-feedback has-error' : '' }}">
    {!! Form::label('tipo_rh', 'Tipo Rh') !!}
    {!! Form::text('tipo_rh', null, ['class' => 'form-control']) !!}
</div>
<!-- Observaciones Field -->
<div class="form-group col-sm-12 col-lg-12 {{ $errors->has('observaciones') ? ' has-feedback has-error' : '' }}">
    {!! Form::label('observaciones', 'Observaciones') !!}
    {!! Form::text('observaciones', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::button('<i class="glyphicon glyphicon-floppy-disk"></i> Guardar', ['type' => 'submit', 'class' => 'btn btn-primary btn-flat']) !!}
    <a href="{!! route('estudiantes.index') !!}" class="btn btn-default btn-flat">Cancelar</a>
</div>