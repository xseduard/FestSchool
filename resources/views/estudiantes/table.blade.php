<table class="table table-responsive table-hover" id="estudiantes-table">
    <thead>
        <th>Documento de Identificación</th>
        <th>Nombres</th>
        <th>Apellidos</th>
        <th>Grado</th>
        <th>Edad</th>
        <th>Tipo Rh</th>
        <th colspan="3" class="text-right">Acciones</th>
    </thead>
    <tbody>
    @foreach($estudiantes as $estudiante)
        <tr>
            <td>{!! $estudiante->di !!}</td>
            <td>{!! $estudiante->nombres !!}</td>
            <td>{!! $estudiante->apellidos !!}</td>
            <td>{!! $estudiante->grado, 'º-', $estudiante->grupo !!}</td>
            <td>{!! $estudiante->edad !!}</td>
            <td>{!! $estudiante->tipo_rh !!}</td>
            <td>
                {!! Form::open(['route' => ['estudiantes.destroy', $estudiante->id], 'method' => 'delete']) !!}
                <div class='btn-group pull-right'>
                    <a href="{!! route('estudiantes.show', [$estudiante->id]) !!}" class='btn btn-default btn-sm'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('estudiantes.edit', [$estudiante->id]) !!}" class='btn btn-default btn-sm' title='Editar'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', [
                        'type' => 'submit',
                        'class' => 'btn btn-danger btn-sm',
                        'onclick' => "return confirm('¿Confirma que desea eliminar?')",
                        'title' => 'Eliminar'
                        ]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>