@extends('layouts.app')

@section('title', 'Generar informe de calificaciones |')

@section('content')
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-8">
            <h2>Generar informe de calificaciones</h2>
            <ol class="breadcrumb">
                <li>
                    <a href="{{ route('home') }}">Inicio</a>
                </li>
                <li>
                    <a href="{!! route('estudiantes.index') !!}">Estudiantes</a>
                </li>
                <li class="active">
                    <strong>Informe</strong>
                </li>                      
            </ol>
        </div>
    </div>
    <div class="content">
        <div class="row wrapper wrapper-content animated fadeInRight">
            <div class="ibox">
                @include('common.errors')
                <div class="ibox-content">
                    <div class="row">
                        {!! Form::open(['route' => 'informe']) !!}

                            @include('estudiantes.informe_fields')

                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@include('common.partial-select-datepicker')