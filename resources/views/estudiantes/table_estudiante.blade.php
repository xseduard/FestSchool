<table class="table table-responsive table-hover" id="estudiantes-table">
    <thead>
        <th>Documento de Identificación</th>
        <th>Nombres</th>
        <th>Apellidos</th>
        <th>Grado</th>
        <th>Edad</th>
        <th>Tipo Rh</th>
        <th colspan="3" class="text-right">Acciones</th>
    </thead>
    <tbody>
    @foreach($estudiantes as $estudiante)
        <tr>
            <td>{!! $estudiante->di !!}</td>
            <td>{!! $estudiante->nombres !!}</td>
            <td>{!! $estudiante->apellidos !!}</td>
            <td>{!! $estudiante->grado, 'º-', $estudiante->grupo !!}</td>
            <td>{!! $estudiante->edad !!}</td>
            <td>{!! $estudiante->tipo_rh !!}</td>
            <td>
               
                <div class='btn-group pull-right'>
                    <a href="{!! route('notas.indexByDocente', ['estudiante' => $estudiante->id, 'asignatura' => $asignatura->id]) !!}" class='btn btn-success btn-sm'><i class="fa fa-arrow-right"></i> Ver notas</a>
                    <a href="{!! route('notas.createByDocente', ['estudiante' => $estudiante->id, 'asignatura' => $asignatura->id]) !!}" class='btn btn-info btn-sm'><i class="fa fa-edit"></i> Calificar</a>
                </div>
            </td>
        </tr>
    @endforeach
    </tbody>
</table>