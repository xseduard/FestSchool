<!-- estudiante_id Selector -->
<div class="form-group col-sm-4 col-lg-4 {{ $errors->has('estudiante_id') ? ' has-feedback has-error' : '' }}">
    {!! Form::label('estudiante_id', 'Estudiante') !!}
    {!! Form::select('estudiante_id', $sels['estudiante_id'], null, ['class' => 'form-control select2', 'style' => 'width: 100%', 'placeholder'=>'Seleccione...*']) !!}
</div>


<!-- Periodo Selector -->
<div class="form-group col-sm-4 col-lg-4 {{ $errors->has('periodo') ? ' has-feedback has-error' : '' }}">
    {!! Form::label('periodo', 'Periodo') !!}
    {!! Form::select('periodo', $sels['periodo'], null, ['class' => 'form-control select2', 'style' => 'width: 100%', 'placeholder'=>'Seleccione...*']) !!}
</div>

<!-- Ano Selector -->
<div class="form-group col-sm-4 col-lg-4 {{ $errors->has('ano') ? ' has-feedback has-error' : '' }}">
    {!! Form::label('ano', 'Año') !!}
     {!! Form::text('ano', 2017, ['class' => 'form-control', 'readonly']) !!}
</div>


<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::button('<i class="fa fa-arrow-right"></i> Generar', ['type' => 'submit', 'class' => 'btn btn-primary btn-flat']) !!}
    <a href="{!! route('estudiantes.index') !!}" class="btn btn-default btn-flat">Cancelar</a>
</div>