<table class="table table-responsive table-hover" id="docentes-table">
    <thead>
        <th>Cédula</th>
        <th>Nombres</th>
        <th>Apellidos</th>
        <th>Fecha Nacimiento</th>
        <th>Direccion</th>
        <th>Telefono</th>
        <th>Celular</th>
        <th>Afinadad</th>
        <th colspan="3" class="text-right">Acciones</th>
    </thead>
    <tbody>
    @foreach($docentes as $docente)
        <tr>
            <td>{!! $docente->Cedula !!}</td>
            <td>{!! $docente->nombres !!}</td>
            <td>{!! $docente->apellidos !!}</td>
            <td>{!! $docente->fecha_nacimiento !!}</td>
            <td>{!! $docente->direccion !!}</td>
            <td>{!! $docente->telefono !!}</td>
            <td>{!! $docente->celular !!}</td>
            <td>{!! $docente->afinadad !!}</td>
            <td>
                {!! Form::open(['route' => ['docentes.destroy', $docente->id], 'method' => 'delete']) !!}
                <div class='btn-group pull-right'>
                    <a href="{!! route('docentes.show', [$docente->id]) !!}" class='btn btn-default btn-sm'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('docentes.edit', [$docente->id]) !!}" class='btn btn-default btn-sm' title='Editar'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', [
                        'type' => 'submit',
                        'class' => 'btn btn-danger btn-sm',
                        'onclick' => "return confirm('¿Confirma que desea eliminar?')",
                        'title' => 'Eliminar'
                        ]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>