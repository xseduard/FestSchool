<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $docente->id !!}</p>
</div>

<!-- Nombres Field -->
<div class="form-group">
    {!! Form::label('nombres', 'Nombres:') !!}
    <p>{!! $docente->nombres !!}</p>
</div>

<!-- Apellidos Field -->
<div class="form-group">
    {!! Form::label('apellidos', 'Apellidos:') !!}
    <p>{!! $docente->apellidos !!}</p>
</div>

<!-- Fecha Nacimiento Field -->
<div class="form-group">
    {!! Form::label('fecha_nacimiento', 'Fecha Nacimiento:') !!}
    <p>{!! $docente->fecha_nacimiento !!}</p>
</div>

<!-- Direccion Field -->
<div class="form-group">
    {!! Form::label('direccion', 'Direccion:') !!}
    <p>{!! $docente->direccion !!}</p>
</div>

<!-- Municipio Id Field -->
<div class="form-group">
    {!! Form::label('municipio_id', 'Municipio Id:') !!}
    <p>{!! $docente->municipio_id !!}</p>
</div>

<!-- Telefono Field -->
<div class="form-group">
    {!! Form::label('telefono', 'Telefono:') !!}
    <p>{!! $docente->telefono !!}</p>
</div>

<!-- Celular Field -->
<div class="form-group">
    {!! Form::label('celular', 'Celular:') !!}
    <p>{!! $docente->celular !!}</p>
</div>

<!-- Afinadad Field -->
<div class="form-group">
    {!! Form::label('afinadad', 'Afinadad:') !!}
    <p>{!! $docente->afinadad !!}</p>
</div>

<!-- Cedula Field -->
<div class="form-group">
    {!! Form::label('Cedula', 'Cedula:') !!}
    <p>{!! $docente->Cedula !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{!! $docente->created_at !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{!! $docente->updated_at !!}</p>
</div>

