
<!-- Cedula Field -->
<div class="form-group col-sm-6 col-lg-6 {{ $errors->has('Cedula') ? ' has-feedback has-error' : '' }}">
    {!! Form::label('Cedula', 'Cedula') !!}
    {!! Form::number('Cedula', null, ['class' => 'form-control']) !!}
</div>


<!-- Nombres Field -->
<div class="form-group col-sm-6 col-lg-6 {{ $errors->has('nombres') ? ' has-feedback has-error' : '' }}">
    {!! Form::label('nombres', 'Nombres') !!}
    {!! Form::text('nombres', null, ['class' => 'form-control']) !!}
</div>

<!-- Apellidos Field -->
<div class="form-group col-sm-6 col-lg-6 {{ $errors->has('apellidos') ? ' has-feedback has-error' : '' }}">
    {!! Form::label('apellidos', 'Apellidos') !!}
    {!! Form::text('apellidos', null, ['class' => 'form-control']) !!}
</div>

<!-- Fecha Nacimiento campo de fecha -->
<div class="form-group col-sm-6 col-lg-6 {{ $errors->has('fecha_nacimiento') ? ' has-feedback has-error' : '' }}">
    {!! Form::label('fecha_nacimiento', 'Fecha Nacimiento') !!}
    <div class="input-group">
        <div class="input-group-addon"><i class="fa fa-calendar"></i></div>
    	{!! Form::text('fecha_nacimiento', null, ['class' => 'form-control datepicker', 'placeholder' => 'AAAA-MM-DD']) !!}
    </div>
</div>

<!-- Direccion Field -->
<div class="form-group col-sm-6 col-lg-6 {{ $errors->has('direccion') ? ' has-feedback has-error' : '' }}">
    {!! Form::label('direccion', 'Direccion') !!}
    {!! Form::text('direccion', null, ['class' => 'form-control']) !!}
</div>

<!-- Municipio Id Selector -->
<div class="form-group col-sm-6 col-lg-6 {{ $errors->has('municipio_id') ? ' has-feedback has-error' : '' }}">
    {!! Form::label('municipio_id', 'Municipio') !!}
    {!! Form::select('municipio_id', $sels['municipio_id'], null, ['class' => 'form-control select2', 'style' => 'width: 100%', 'placeholder'=>'Seleccione...*']) !!}
</div>

<!-- Telefono Field -->
<div class="form-group col-sm-6 col-lg-6 {{ $errors->has('telefono') ? ' has-feedback has-error' : '' }}">
    {!! Form::label('telefono', 'Telefono') !!}
    {!! Form::number('telefono', null, ['class' => 'form-control']) !!}
</div>

<!-- Celular Field -->
<div class="form-group col-sm-6 col-lg-6 {{ $errors->has('celular') ? ' has-feedback has-error' : '' }}">
    {!! Form::label('celular', 'Celular') !!}
    {!! Form::number('celular', null, ['class' => 'form-control']) !!}
</div>

<!-- Afinadad Field -->
<div class="form-group col-sm-12 col-lg-12 {{ $errors->has('afinadad') ? ' has-feedback has-error' : '' }}">
    {!! Form::label('afinadad', 'Afinadad') !!}
    {!! Form::textarea('afinadad', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::button('<i class="glyphicon glyphicon-floppy-disk"></i> Guardar', ['type' => 'submit', 'class' => 'btn btn-primary btn-flat']) !!}
    <a href="{!! route('docentes.index') !!}" class="btn btn-default btn-flat">Cancelar</a>
</div>