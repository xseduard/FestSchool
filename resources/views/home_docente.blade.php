@extends('layouts.app_docente')

@section('title', 'Inicio |')
@section('topnavbar-class', 'white-bg')

@section('content')
{{--     <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-8">
            <h2>FestCar Home</h2>
            <ol class="breadcrumb">
                <li>
                  <strong>Inicio</strong>
                </li>                  
            </ol>
        </div>        
    </div> --}}
    <div class="content">
            @include('flash::message')    
            @include('common.errors')
            <div class="clearfix"></div>
           <!-- contenido -->
     
       
      <div class="row">
        <div class="col-sm-12">
              <center><img style="margin-top: 30px; height: 180px" src="{{ asset('/multimedia/web/logo-white-v2.png') }}"> </center>
          </div>
         <div class="col-sm-offset-2 col-md-8">
            <!-- LINE CHART -->
            <div class="jumbotron" style="margin-top: 30px">
                <h1>Docente</h1>
                <p>Sistema de información para la gestion y control de instituciones educativas.</p>
                {{-- <p><a role="button" class="btn btn-primary btn-lg startTour">Iniciar recorrido</a> --}}
                
            </div>
            <!-- /.box -->
          </div>
          
          
       </div>
       <div class="row">
        <div class="col-sm-offset-2 col-sm-8">
         <div class="ibox">      
                @include('flash::message')
                <div class="ibox-content table-responsive">
                    <p>Nombre</p>
                    <h1>{{ $docente->fullname }}</h1>
                    <p>cédula</p>
                    <h1>{{ $docente->Cedula }}</h1>
                    <p>Fecha de nacimiento</p>
                    <h1>{{ $docente->fecha_nacimiento->format('d / F / Y') }} - ( {{ $docente->fecha_nacimiento->diffForHumans() }} )</h1>
                    <p>Dirección</p>
                    <h1>{{ $docente->direccion }}</h1>
                    <p>Contacto</p>
                    <h1>{{ $docente->telefono }} - {{ $docente->celular }}</h1>
                </div>
            </div>
       </div>
      </div>
    </div>

@endsection

@push('css')
  <link href="/src/css/bootstrapTour/bootstrap-tour.min.css" rel="stylesheet">
@endpush

@section('scripts')
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.6.0/Chart.bundle.min.js"></script>
    <script src="/src/js/bootstrapTour/bootstrap-tour.min.js"></script>
   
    
@endsection
