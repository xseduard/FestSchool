@extends('layouts.app')

@section('title', 'Inicio |')
@section('topnavbar-class', 'white-bg')

@section('content')
{{--     <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-8">
            <h2>FestCar Home</h2>
            <ol class="breadcrumb">
                <li>
                  <strong>Inicio</strong>
                </li>                  
            </ol>
        </div>        
    </div> --}}
    <div class="content">
            @include('flash::message')    
            @include('common.errors')
            <div class="clearfix"></div>
           <!-- contenido -->
     
       
      <div class="row">
        <div class="col-sm-12">
              <img style="margin-top: 30px; width: 90%" src="{{ asset('/multimedia/web/logo-white-v2.png') }}">
          </div>
         <div class="col-md-12">
            <!-- LINE CHART -->
            <div class="jumbotron" style="margin-top: 30px">
                <h1>HarvardColege on FestSchool</h1>
                <p>Sistema de información para la gestion y control de instituciones educativas.</p>
                {{-- <p><a role="button" class="btn btn-primary btn-lg startTour">Iniciar recorrido</a> --}}
                
            </div>
            <!-- /.box -->
          </div>
          
          
       </div>
    </div>

@endsection

@push('css')
  <link href="/src/css/bootstrapTour/bootstrap-tour.min.css" rel="stylesheet">
@endpush

@section('scripts')
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.6.0/Chart.bundle.min.js"></script>
    <script src="/src/js/bootstrapTour/bootstrap-tour.min.js"></script>
   
    
@endsection
