<table class="table table-responsive table-hover" id="logros-table">
    <thead>
        <th>Asignatura</th>
        <th>Período</th>
        <th>Logro</th>
        <th colspan="3" class="text-right">Acciones</th>
    </thead>
    <tbody>
    @foreach($logros as $logro)
        <tr>
            <td>{!! $logro->asignatura->nombre !!}</td>
            <td>{!! $periodo[$logro->periodo] !!}</td>
            <td>{!! $logro->logro !!}</td>
            <td>
                {!! Form::open(['route' => ['logros.destroy', $logro->id], 'method' => 'delete']) !!}
                <div class='btn-group pull-right'>
                   {{--  <a href="{!! route('logros.show', [$logro->id]) !!}" class='btn btn-default btn-sm'><i class="glyphicon glyphicon-eye-open"></i></a> --}}
                    <a href="{!! route('logros.edit', [$logro->id]) !!}" class='btn btn-default btn-sm' title='Editar'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', [
                        'type' => 'submit',
                        'class' => 'btn btn-danger btn-sm',
                        'onclick' => "return confirm('¿Confirma que desea eliminar?')",
                        'title' => 'Eliminar'
                        ]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>