<!-- Asignatura Id Selector -->
<div class="form-group col-sm-6 col-lg-6 {{ $errors->has('asignatura_id') ? ' has-feedback has-error' : '' }}">
    {!! Form::label('asignatura_id', 'Asignatura') !!}
    {!! Form::select('asignatura_id', $sels['asignatura_id'], null, ['class' => 'form-control select2', 'style' => 'width: 100%', 'placeholder'=>'Seleccione...*']) !!}
</div>

<!-- Periodo Selector -->
<div class="form-group col-sm-6 col-lg-6 {{ $errors->has('periodo') ? ' has-feedback has-error' : '' }}">
    {!! Form::label('periodo', 'Periodo') !!}
    {!! Form::select('periodo', $sels['periodo'], null, ['class' => 'form-control select2', 'style' => 'width: 100%', 'placeholder'=>'Seleccione...*']) !!}
</div>

<!-- Logro Field -->
<div class="form-group col-sm-12 col-lg-12 {{ $errors->has('logro') ? ' has-feedback has-error' : '' }}">
    {!! Form::label('logro', 'Logros') !!}
    {!! Form::textarea('logro', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::button('<i class="glyphicon glyphicon-floppy-disk"></i> Guardar', ['type' => 'submit', 'class' => 'btn btn-primary btn-flat']) !!}
    <a href="{!! route('logros.index') !!}" class="btn btn-default btn-flat">Cancelar</a>
</div>