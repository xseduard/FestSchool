<table class="table table-responsive table-hover" id="impartes-table">
    <thead>
        <th>Docente Id</th>
        <th>Asignatura Id</th>
        <th>Ano</th>
        <th colspan="3" class="text-right">Acciones</th>
    </thead>
    <tbody>
    @foreach($impartes as $imparte)
        <tr>
            <td>{!! $imparte->docente->fullname !!}</td>
            <td>{!! $imparte->asignatura->nombre !!}</td>
            <td>{!! $imparte->ano !!}</td>
            <td>
                {!! Form::open(['route' => ['impartes.destroy', $imparte->id], 'method' => 'delete']) !!}
                <div class='btn-group pull-right'>
                   {{--  <a href="{!! route('impartes.show', [$imparte->id]) !!}" class='btn btn-default btn-sm'><i class="glyphicon glyphicon-eye-open"></i></a> --}}
                    <a href="{!! route('impartes.edit', [$imparte->id]) !!}" class='btn btn-default btn-sm' title='Editar'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', [
                        'type' => 'submit',
                        'class' => 'btn btn-danger btn-sm',
                        'onclick' => "return confirm('¿Confirma que desea eliminar?')",
                        'title' => 'Eliminar'
                        ]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>