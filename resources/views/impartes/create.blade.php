@extends('layouts.app')

@section('title', 'Impartes |')

@section('content')
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-8">
            <h2>Impartes</h2>
            <ol class="breadcrumb">
                <li>
                    <a href="{{ route('home') }}">Inicio</a>
                </li>
                <li>
                    <a href="{!! route('impartes.index') !!}">Impartes</a>
                </li>
                <li class="active">
                    <strong>Registrar</strong>
                </li>                      
            </ol>
        </div>
    </div>
    <div class="content">
        <div class="row wrapper wrapper-content animated fadeInRight">
            <div class="ibox">
                @include('common.errors')
                <div class="ibox-content">
                    <div class="row">
                        {!! Form::open(['route' => 'impartes.store']) !!}

                            @include('impartes.fields')

                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@include('common.partial-select-datepicker')