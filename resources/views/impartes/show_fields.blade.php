<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $imparte->id !!}</p>
</div>

<!-- Docente Id Field -->
<div class="form-group">
    {!! Form::label('docente_id', 'Docente Id:') !!}
    <p>{!! $imparte->docente_id !!}</p>
</div>

<!-- Asignatura Id Field -->
<div class="form-group">
    {!! Form::label('asignatura_id', 'Asignatura Id:') !!}
    <p>{!! $imparte->asignatura_id !!}</p>
</div>

<!-- Ano Field -->
<div class="form-group">
    {!! Form::label('ano', 'Ano:') !!}
    <p>{!! $imparte->ano !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{!! $imparte->created_at !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{!! $imparte->updated_at !!}</p>
</div>

