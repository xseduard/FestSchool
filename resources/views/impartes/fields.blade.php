<!-- Docente Id Selector -->
<div class="form-group col-sm-6 col-lg-6 {{ $errors->has('docente_id') ? ' has-feedback has-error' : '' }}">
    {!! Form::label('docente_id', 'Docente') !!}
    {!! Form::select('docente_id', $sels['docente_id'], null, ['class' => 'form-control select2', 'style' => 'width: 100%', 'placeholder'=>'Seleccione...*']) !!}
</div>

<!-- Asignatura Id Selector -->
<div class="form-group col-sm-6 col-lg-6 {{ $errors->has('asignatura_id') ? ' has-feedback has-error' : '' }}">
    {!! Form::label('asignatura_id', 'Asignatura') !!}
    {!! Form::select('asignatura_id', $sels['asignatura_id'], null, ['class' => 'form-control select2', 'style' => 'width: 100%', 'placeholder'=>'Seleccione...*']) !!}
</div>

<!-- Ano Field -->
<div class="form-group col-sm-6 col-lg-6 {{ $errors->has('ano') ? ' has-feedback has-error' : '' }}">
    {!! Form::label('ano', 'Año') !!}
    {!! Form::text('ano', 2017, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::button('<i class="glyphicon glyphicon-floppy-disk"></i> Guardar', ['type' => 'submit', 'class' => 'btn btn-primary btn-flat']) !!}
    <a href="{!! route('impartes.index') !!}" class="btn btn-default btn-flat">Cancelar</a>
</div>