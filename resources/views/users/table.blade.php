<table class="table table-hover" id="users-table">
    <thead>
    @if (auth()->user()->isAdmin() && auth()->id() === 1)
        <th>Imper</th>
    @endif
        <th>Avatar</th>
        <th>Cédula</th>
        <th>Nombres</th>
        <th>Apellidos</th>
        <th>Email</th>
        <th>Roles</th>
        <th>Docente</th>
        <th colspan="3" class="text-right">Acciones</th>
    </thead>
    <tbody>
    @foreach($users as $user)
        <tr>
            <td>
                @if (auth()->user()->isAdmin() && auth()->id() === 1 && $user->id != 1)                    
                        {!! Form::open(['route' => ['impersonations.store'], 'method' => 'POST']) !!}
                            {!! Form::hidden('user_id',$user->id) !!}
                            {!! Form::button('<i class="fa fa-user-secret"></i>', [
                                'type' => 'submit',
                                'class' => 'btn btn-default btn-sm',
                                'title' => 'Personificar',
                                'style' => 'position'
                                ]) !!}                        

                    {!! Form::close() !!}
                @else
                    @if (auth()->user()->isAdmin() && auth()->id() === 1 && $user->id === 1)
                        <a href="#" class="btn btn-default btn-sm" disabled><i class="fa fa-lock"></i></a>
                    @endif
                @endif

            </td>
            <td title="Ultima actualización: {{ $user->created_at->diffForHumans() }}">
                @if (!is_null($user->avatar))
                    <img src="{{ $user->avatar }}" alt="Profile" class="img-circle" width="40px">               
                @endif               
                
            </td>
            <td>{!! number_format($user->cedula, 0, '.', '.' ) !!}</td>
            <td>{!! $user->nombres !!}</td>
            <td>{!! $user->apellidos !!}</td>
            <td>{!! $user->email !!}</td>
            <td>{{  $user->roles->pluck('display_name')->implode(', ') }}</td>
            <td>{!!  optional($user->docente)->nombres ? '<span class="label label-primary">Si</span>' : '<span class="label label-default">No</span>' !!}</td>
            <td width="90px">
                <div class='pull-right'>
                    {!! Form::open(['route' => ['usuarios.destroy', $user->id], 'method' => 'delete']) !!}
                       {{--  <a href="{!! route('users.show', [$user->id]) !!}" class='btn btn-default btn-sm'><i class="glyphicon glyphicon-eye-open"></i></a> --}}
                        <a href="{!! route('usuarios.edit', [$user->id]) !!}" class='btn btn-default btn-sm' data-toggle='tooltip' title='' data-original-title='Editar'><i class="zmdi zmdi-edit"></i></a>
                        {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', [
                            'type' => 'submit',
                            'class' => 'btn btn-outline btn-danger btn-sm',
                            'onclick' => "return confirm('¿Confirma que desea eliminar?')",
                            'title' => 'Eliminar'
                            ]) !!}
                    {!! Form::close() !!}
                    
                </div>
            </td>
        </tr>
    @endforeach
    </tbody>
</table>