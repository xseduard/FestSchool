<!-- Cedula Field -->
<div class="form-group">
    {!! Form::label('cedula', 'Cédula', ['class' => 'col-sm-2 control-label']) !!}
    <div class="col-sm-10">
        {!! Form::text('cedula', null, ['class' => 'form-control']) !!}
    </div>
</div>

<!-- Nombres Field -->
<div class="form-group">
    {!! Form::label('nombres', 'Nombres', ['class' => 'col-sm-2 control-label']) !!}
    <div class="col-sm-10">
        {!! Form::text('nombres', null, ['class' => 'form-control']) !!}
    </div>
</div>

<!-- Apellidos Field -->
<div class="form-group">
    {!! Form::label('apellidos', 'Apellidos', ['class' => 'col-sm-2 control-label']) !!}
    <div class="col-sm-10">
        {!! Form::text('apellidos', null, ['class' => 'form-control']) !!}
    </div>
</div>

<!-- Email Field -->
<div class="form-group">
    {!! Form::label('email', 'Email', ['class' => 'col-sm-2 control-label']) !!}
    <div class="col-sm-10">
        {!! Form::email('email', null, ['class' => 'form-control']) !!}
    </div>
</div>

<div class="clearfix"></div>

@if (Request::is('usuarios/create'))
    <!-- Password Field -->
    <div class="form-group">
        {!! Form::label('password', 'Contraseña', ['class' => 'col-sm-2 control-label']) !!}
        <div class="col-sm-10">
            {!! Form::password('password', ['class' => 'form-control']) !!}
        </div>
    </div>

    <!-- Password Field -->
    <div class="form-group">
        {!! Form::label('password_confirmation', 'Confirmar Contraseña', ['class' => 'col-sm-2 control-label']) !!}
        <div class="col-sm-10">
            {!! Form::password('password_confirmation', ['class' => 'form-control']) !!}
        </div>
    </div>
@endif

@if (auth()->user()->isAdmin())
    <!-- Roles Field -->
    <div class="form-group">
        {!! Form::label('roles', 'Roles', ['class' => 'col-sm-2 control-label']) !!}
        <div class="col-sm-10">            
            @foreach ($roles as $id => $name)
                <label class="checkbox-inline">  
                @if (isset($user))                    
                    {!! Form::checkbox('roles[]', $id,
                    $user->roles->pluck('id')->contains($id) ? '1' : null
                    ) !!} 
                    {{ $name }}
                @else
                    {!! Form::checkbox('roles[]', $id, false) !!} 
                    {{ $name }}                 
                @endif
                </label>
            @endforeach
        </div>
    </div>
@endif

@unless (Request::is('usuarios/create*'))
<div class="form-group">
    {!! Form::label('avatar', 'Avatar', ['class' => 'col-sm-2 control-label']) !!}
    <div class="col-sm-10">
        @if ($user->avatar != '/default-profile.png')
            <a href="{!! route('usuarios.reset.image', [$user->id]) !!}" data-toggle='tooltip' title='' data-original-title='Restaurar avatar'>
                <img src="{{ $user->avatar }}" class="img-circle" alt="Avatar Image" width="80px" style="margin-bottom: 5px;  border: 3px solid;  border-color: rgba(0,0,0,0.2);">
            </a>
        @else
            <img src="{{ $user->avatar }}" class="img-circle" alt="Avatar Image" width="80px" style="margin-bottom: 5px;  border: 3px solid;  border-color: rgba(0,0,0,0.2);">
        @endif
        
        {!! Form::file('avatar', ['class' => '']) !!}
    </div>
</div>
@endunless


<!-- Submit Field -->

<div class="form-group">
   <div class="col-sm-10 col-sm-offset-2">     
        {!! Form::button('<i class="glyphicon glyphicon-floppy-disk"></i> Guardar', ['type' => 'submit', 'class' => 'btn btn-primary  ']) !!}
        
        @if (auth()->user()->isMixAdmin())
            <a href="{!! route('usuarios.index') !!}" class="btn btn-default  ">Cancelar</a>
        @endif
        
   </div>
 </div>



{{-- @push('css')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/dropzone/5.1.1/min/dropzone.min.css">
@endpush --}}
@push('scripts')
{{-- <script src="https://cdnjs.cloudflare.com/ajax/libs/dropzone/5.1.1/min/dropzone.min.js"></script> --}}
  <script>
    $(function () {
        $('input').iCheck({
            checkboxClass: 'icheckbox_square-blue',
            radioClass: 'iradio_square-blue',
            increaseArea: '20%' // optional
        });
    });
  </script>
@endpush