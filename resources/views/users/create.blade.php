@extends('layouts.app')

@section('title', 'Usuarios |')

@section('content')
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-8">
            <h2>Cuentas de usuario</h2>
            <ol class="breadcrumb">
                <li>
                    <a href="{{ route('home') }}">Inicio</a>
                </li>
                <li>
                    <a href="{{ route('usuarios.index') }}">Usuarios</a>
                </li>
                <li class="active">
                    <strong>Registrar</strong>
                </li>                        
            </ol>
        </div>
    </div>
    <div class="content">
        <div class="row wrapper wrapper-content animated fadeInRight"> 
            <div class="ibox">
                @include('common.errors')
                <div class="ibox-content">
                    <div class="row">
                        {!! Form::open(['route' => 'usuarios.store', 'enctype' => 'multipart/form-data', 'class' => 'form-horizontal']) !!}

                            @include('users.fields')

                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('css')
    <link rel="stylesheet" href="/src/icheck/skins-flat/green.css">
@endpush

@push('scripts')
 <script src="/src/icheck/icheck.min.js"></script>
 <script>
    $(document).ready(function () {  
         $('input').iCheck({
            checkboxClass: 'icheckbox_flat-green',
            radioClass: 'iradio_flat-green',
            increaseArea: '20%' // optional
        });
    });
    
 </script>
@endpush
