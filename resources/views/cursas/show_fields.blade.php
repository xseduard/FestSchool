<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $cursa->id !!}</p>
</div>

<!-- Estudiante Id Field -->
<div class="form-group">
    {!! Form::label('estudiante_id', 'Estudiante Id:') !!}
    <p>{!! $cursa->estudiante_id !!}</p>
</div>

<!-- Asignatura Id Field -->
<div class="form-group">
    {!! Form::label('asignatura_id', 'Asignatura Id:') !!}
    <p>{!! $cursa->asignatura_id !!}</p>
</div>

<!-- Ano Field -->
<div class="form-group">
    {!! Form::label('ano', 'Ano:') !!}
    <p>{!! $cursa->ano !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{!! $cursa->created_at !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{!! $cursa->updated_at !!}</p>
</div>

