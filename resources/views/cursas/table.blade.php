<table class="table table-responsive table-hover" id="cursas-table">
    <thead>
        <th>Estudiante</th>
        <th>Asignatura</th>
        <th>Año</th>
        <th colspan="3" class="text-right">Acciones</th>
    </thead>
    <tbody>
    @foreach($cursas as $cursa)
        <tr>
            <td>{!! $cursa->estudiante->fullname !!}</td>
            <td>{!! $cursa->asignatura->nombre !!}</td>
            <td><div class="label label-primary">{!! $cursa->ano !!}</div></td>
            <td>
                {!! Form::open(['route' => ['cursas.destroy', $cursa->id], 'method' => 'delete']) !!}
                <div class='btn-group pull-right'>
                   {{--  <a href="{!! route('cursas.show', [$cursa->id]) !!}" class='btn btn-default btn-sm'><i class="glyphicon glyphicon-eye-open"></i></a> --}}
                    <a href="{!! route('cursas.edit', [$cursa->id]) !!}" class='btn btn-default btn-sm' title='Editar'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', [
                        'type' => 'submit',
                        'class' => 'btn btn-danger btn-sm',
                        'onclick' => "return confirm('¿Confirma que desea eliminar?')",
                        'title' => 'Eliminar'
                        ]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>