
<div class="row">

	<div class="col-sm-12 col-xl-12">
	  <!-- Box Comment -->


	    <!-- /.box-header -->
	    <div class="ibox-content m-b-sm border-bottom">
 		
	    	<div class="row">
		    	
				<div class="form-group col-xl-2 col-sm-4">
				    {!! Form::label('vehiculo_id', 'Placa') !!}
				    {!! Form::text('vehiculo_id', null, ['class' => 'form-control text-right', 'placeholder' => 'AAA000']) !!}
				</div>
				<div class="form-group col-xl-2 col-sm-4">
				    {!! Form::label('estado', 'Estado') !!}
				    {!! Form::select('estado',['vigente' => 'Vigente', 'no_vigente' => 'No Vigente'], null, ['class' => 'form-control select2_without_search', 'style' => 'width: 100%', 'placeholder'=>'Seleccione...*']) !!}
				</div>			
				<div class="form-group col-xl-2 col-sm-4">
				    {!! Form::label('order_item', 'Ordenar por') !!}
				    {!! Form::select('order_item', ['updated_at' => 'Última actualización',						
						'vehiculo_id'           => 'Vehículo',
						'fecha_vigencia_inicio' => 'Fecha Vigencia Inicio',
						'fecha_vigencia_final'  => 'Fecha Vigencia Final',
						'created_at'            => 'Fecha de Registro',
				      ], 'updated_at', ['class' => 'form-control select2_without_search', 'required', 'style' => 'width: 100%', 'placeholder'=>'Seleccione...*']) !!}		    
				</div>
				<div class="form-group col-xl-2 col-sm-4">
				    {!! Form::label('order_type', 'Tipo de orden') !!}
				    {!! Form::select('order_type',['desc' => 'Descendente', 'asc' => 'Ascendente'], 'desc', ['class' => 'form-control select2_without_search','required', 'style' => 'width: 100%', 'placeholder'=>'Seleccione...*']) !!}		    
				</div>
				<div class="form-group col-xl-2 col-sm-2">
				    {!! Form::label('per_page', 'Cant. Registros') !!}
				    {!! Form::number('per_page', null, ['class' => 'form-control text-right', 'placeholder' => '15']) !!}
				</div>				
	    	</div>   
	    
              <button type="submit" class="btn btn-primary btn-flat"><i class="fa fa-search"></i> Aplicar Filtros</button> 
              {!! $records->count() !!} de {!! $records->total() !!} Registros
         </div>
	    <!-- /.box-footer -->

	</div>

</div>




