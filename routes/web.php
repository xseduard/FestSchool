<?php

use Illuminate\Support\Facades\Session;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
// Ver SQL Optimizador

// DB::Listen(Function($query){
//     echo "<pre>{ $query->sql }</pre>";
//     //echo "<pre>{ $query->time }</pre>";
// });

Route::get('alert', function () { 
	Session::flash('error2', 'hola');
	return view('home');
	 });

Route::get('400', function () { abort(400); });
Route::get('401', function () { abort(401); });
Route::get('403', function () { abort(403); });
Route::get('404', function () { abort(404); });
Route::get('500', function () { abort(500); });
Route::get('504', function () { abort(504); });
Route::get('509', function () { abort(509); });

Route::get('/', function () {
  return redirect('home');
});

Route::get('/invitado', function () {
  return 'Hola';
});


Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/ver/estudiantes', 'HomeController@index')->name('ver.estudiantes');

Route::get('/ver/asignaturas', 'HomeController@asignaturas')->name('ver.asignaturas');

Route::get('/informe/estudiante/crear', 'EstudianteController@informe_crear')->name('informe.crear');

Route::post('/informe/estudiante', 'EstudianteController@informe')->name('informe');


Route::get('/ver/estudiantesByAsignatura/{asignatura}', 'HomeController@estudiantesByAsignatura')->name('ver.estudiantesByAsignatura');

Route::get('/notas/createByDocente/{asignatura}/{estudiante}', 'NotaController@createByDocente')->name('notas.createByDocente');

Route::get('/notas/indexByDocente/{asignatura}/{estudiante}', 'NotaController@indexByDocente')->name('notas.indexByDocente');




Route::resource('usuarios', 'UsersController');

Route::get('usuarios/gen/{id}',[
            'as' => 'usuarios.genUser',
            'uses' => 'UsersController@genUser',
        ]);

Route::get('usuarios/reset/{id}',[
            'as' => 'usuarios.reset.image',
            'uses' => 'UsersController@resetImage',
        ]);



Route::resource('roles', 'RoleController');

Route::post('impersonations', 'ImpersonationController@store')->name('impersonations.store');
Route::delete('impersonations', 'ImpersonationController@destroy')->name('impersonations.destroy');


// Registration Routes...

        // Route::get('register', function () { abort(404); });  //Desactivar Registro
        // Route::post('register', function () { abort(404); });  //Desactivar Registro

// END ADMIN

    Route::resource('departamentos', 'DepartamentoController');
    Route::resource('municipios', 'MunicipioController');








Route::resource('sedes', 'SedeController');

Route::resource('estudiantes', 'EstudianteController');

Route::resource('docentes', 'DocenteController');

Route::resource('asignaturas', 'AsignaturaController');

Route::resource('cursas', 'CursaController');

Route::resource('impartes', 'ImparteController');

Route::resource('logros', 'LogroController');

Route::resource('notas', 'NotaController');